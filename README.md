
# Sylva-capi-cluster

A Helm chart for deploying ClusterAPI and ClusterAPI providers resources

## Configuration

The following table lists the configurable parameters of the Sylva-capi-cluster chart and their default values.

| Parameter                | Description             | Default        |
| ------------------------ | ----------------------- | -------------- |
| `name` | name of the CAPI cluster | `"workload-cluster"` |
| `air_gapped` | can be set to true to do an RKE2 deployment disconnected from the Internet | `false` |
| `cis_profile` | CIS profile to be used. Curently supported only for rke2 clusters. "cis-1.23" for 1.25-1.28, "cis" for 1.25+. Using the generic "cis" profile will ensure that the cluster passes the CIS benchmark associated with the Kubernetes version that RKE2 is running | `""` |
| `cni.default_provider` | default CNI provider | `"calico"` |
| `k8s_version` | kubernetes version to be used; add "+rke2r1" for cabpr (RKE2) clusters | `"v1.24.12"` |
| `capi_providers.infra_provider` | capd, capo, capm3 or capv | mandatory |
| `capi_providers.bootstrap_provider` | cabpr (RKE2), cabpk (kubeadm) or capbpoa(OpenShift) | mandatory |
| `control_plane_replicas` | nodes number for control plane | `3` |
| `cluster_services_cidrs` | network ranges (in CIDR notation) from which Services networks are allocated | `["100.73.0.0/16"]` |
| `cluster_pods_cidrs` | network ranges (in CIDR notation) from which Pod networks are allocated | `["100.72.0.0/16"]` |
| `enable_longhorn` |  | `false` |
| `cluster_virtual_ip` | address for kube-api (and other services) exposure | `"10.122.22.151"` |
| `cluster_public_ip` | (optional) when set, the cluster apiServerFixedIP will be set to this address and CAPI will connect to the cluster via this address (typically used with CAPO when a Floating IP is bound to `cluster_virtual_ip`) | `""` |
| `cluster_primary_interfaces` | list, (only for capm3) will default to [control_plane.capm3.primary_pool_interface] | `[]` |
| `cluster_api_cert_extra_SANs` | (optional) list of extra subjectAltNames to be added to cluster api endpoint certificate, useful if you want to access the api through a NAT or reverse-proxy | `[]` |
| `mgmt_cluster_ip` | management cluster external IP | `"11.11.11.11"` |
| 'metallb_helm_extra_ca_certs' | Additional CA certs to be trusted for pulling metallb helm artifacts (one string in PEM-format, with one or more certificates) | "" |
| `etcd.quota-backend-bytes`|configure the quota of the backend db size (etcd recommendations min: "2147483648", max: "8589934592" )|`"4294967296"`|
| `etcd.auto-compaction-mode`| set to automatically compact the keyspace ["periodic"|"revision"]| `"periodic"` |
| `etcd.auto-compaction-retention`| compaction interval in min or hour (example: 30m or 12h) | `"12h"` |
| `capd.docker_host_socket` | host path extra mount for CAPD controller | `"/var/run/docker.sock"` |
| `capd.image_name` | image used for CABPR (RKE2) CAPD CP & MD nodes | `"registry.gitlab.com/sylva-projects/sylva-elements/container-images/rke2-in-docker:v1-24-12-rke2r1"` |
| `capo.os_image_selector` | OS image selector for CP & MD nodes (dict)<br>see [OS image selection](#os-image-selection) | {} |
| `capo.image_key` | key of image used for CP & MD nodes (string)<br>see [OS image selection](#os-image-selection) |  |
| `capo.image_name` | name of image used for CP & MD nodes (string)<br>see [OS image selection](#os-image-selection) |  |
| `capo.flavor_name` |  | `"m1.large"` |
| `capo.rootVolume` | Let this parameter empty if you don't intend to use root volume for CAPO CP & MD nodes | `{}` |
| `capo.control_plane_az` | list of OpenStack availability zones to deploy control planes nodes to, otherwise all would be candidates | `["replace_me"]` |
| `capo.ssh_key_name` | OpenStack VM SSH key | `"replace_me"` |
| `capo.network_id` | OpenStack network used for nodes and VIP port | `"replace_me"` |
| `capo.clouds_yaml.clouds.capo_cloud.auth.auth_url` | $OS_AUTH_URL | `"https://replace_me"` |
| `capo.clouds_yaml.clouds.capo_cloud.auth.user_domain_name` | $OS_USER_DOMAIN_NAME | `"replace_me"` |
| `capo.clouds_yaml.clouds.capo_cloud.auth.project_domain_name` | $OS_PROJECT_DOMAIN_NAME | `"replace_me"` |
| `capo.clouds_yaml.clouds.capo_cloud.auth.project_name` | $OS_PROJECT_DOMAIN_NAME | `"replace_me"` |
| `capo.clouds_yaml.clouds.capo_cloud.auth.username` | $OS_USERNAME | `"replace_me"` |
| `capo.clouds_yaml.clouds.capo_cloud.auth.password` | $OS_PASSWORD | `"replace_me"` |
| `capo.clouds_yaml.clouds.capo_cloud.region_name` | $OS_REGION_NAME | `"replace_me"` |
| `capo.clouds_yaml.clouds.capo_cloud.verify` | boolean for server cert check | `false` |
| `capo.cacert` | cert used to validate CA of OpenStack APIs | `"sylva-tag"` |
| `capo.resources_tag` | tag set for OpenStack resources | `"sylva-tag"` |
| `capo.extra.allowed_address_pairs` | list of allowed address pairs config configured on control plane node interface | `[]` |
| `capv.username` | vSphere username | `"replace_me"` |
| `capv.password` | vSphere password | `"replace_me"` |
| `capv.dataCenter` | Datacenter to use | `"*"` |
| `capv.image_name` | image used for CAPV CP & MD nodes | `"replace_me"` |
| `capv.diskGiB` | disk size for CP & MD vSphereMachineTemplate | `50` |
| `capv.memoryMiB` | memory size for CP & MD vSphereMachineTemplate | `8192` |
| `capv.numCPUs` | CPUs for CP & MD vSphereMachineTemplate | `4` |
| `capv.networks.default.networkName` | vSphere network name for VMs and CSI | `"replace_me"` |
| `capv.networks.default.dhcp4` | vSphere network DHCPv4 enable boolean | `true` |
| `capv.server` | vSphere server DNS name or IP | `"replace_me"` |
| `capv.dataStore` | vSphere datastore name | `"replace_me"` |
| `capv.tlsThumbprint` | vSphere https TLS thumbprint | `"replace_me"` |
| `capv.ssh_key` | SSH public key for VM access | `"replace_me"` |
| `capv.folder` | vSphere folder | `"replace_me"` |
| `capv.resourcePool` | vSphere resourcepool | `"replace_me"` |
| `capv.storagePolicyName` | vSphere storage policy name | `"replace_me"` |
| `capv.template_clone_mode` | possible values: "fullClone", "linkedClone" | `"fullClone"` |
| `capm3.use_os_image_server_service_urls` | boolean conditioning the machine_image_url to http://os-image-server-<image_key>.os-images.svc.cluster.local:8080/<filename>, to be used in [libvirt-metal](https://gitlab.com/sylva-projects/sylva-elements/container-images/libvirt-metal/-/tree/main) management cluster | `false` |
| `capm3.image_provisioning_host` | hostname/IP on which os-image-server is serving images, defaults to mgmt_cluster_ip value | `"11.11.11.11"` |
| `capm3.primary_pool_name` |  | `"primary-pool"` |
| `capm3.primary_pool_network` |  | `"172.20.36.128"` |
| `capm3.primary_pool_gateway` |  | `"172.20.36.129"` |
| `capm3.primary_pool_end` |  | `"172.20.36.150"` |
| `capm3.primary_pool_start` |  | `"172.20.36.140"` |
| `capm3.primary_pool_prefix` |  | `"26"` |
| `capm3.primary_pool_interface` |  | `""` |
| `capm3.provisioning_pool_name` | optional network definition if needed for the provisioning phase in the case when Ironic is not reachable from the primary interface |  |
| `capm3.provisioning_pool_network` |  |  |
| `capm3.provisioning_pool_gateway` |  |  |
| `capm3.provisioning_pool_end` |  |  |
| `capm3.provisioning_pool_start` |  |  |
| `capm3.provisioning_pool_prefix` |  |  |
| `capm3.provisioning_pool_interface` |  |  |
| `capm3.dns_servers` | list of IPv4 DNS servers to be used by BM CP & MD nodes | `[1.1.1.1, 8.8.8.8]` |
| `capm3.os_image_selector` | OS image selector for CAPO CP & MD nodes (dict)<br>see [OS image selection](#os-image-selection) | {} |
| `capm3.image_key` | key of image used for CAPO CP & MD nodes (string)<br>see [OS image selection](#os-image-selection) |  |
| `capm3.machine_image_url` | URL for BM CP & MD node image on a webserver<br>see [OS image selection](#os-image-selection) | `"http://55.55.55.55/ubuntu-22.04-plain.qcow2"` |
| `capm3.machine_image_format` | format for BM CP & MD node image<br>see [OS image selection](#os-image-selection) |  |
| `capm3.machine_image_checksum` | checksum for BM CP & MD node image, hosted on a webserver<br>see [OS image selection](#os-image-selection) |  |
| `capm3.machine_image_checksum_type` | checksum type for BM CP & MD node image<br>see [OS image selection](#os-image-selection) |  |
| `capm3.nodeReuse` | boolean conditioning CAPM3 Machine controller to pick the same pool of BMHs' that were released during the upgrade operation, see [Metal3MachineTemplate.spec.nodeReuse](https://github.com/metal3-io/metal3-docs/blob/main/design/cluster-api-provider-metal3/node_reuse.md#node-reuse) | `true` |
| `capm3.label_sync_prefixes` | comma separated list of label prefixes that should be copied from BareMetalHosts to nodes (see https://github.com/Nordix/metal3-docs/blob/main/design/sync-labels-bmh-to-node.md) | `{}` |
| `capm3.networkData.provisioning_pool_interface` | [CAPM3 NetworkDataRoutev4 route format](https://github.com/metal3-io/cluster-api-provider-metal3/blob/9d9e9404d835cb7dabdd0a3bbab79ef12c8f36cf/api/v1beta1/metal3datatemplate_types.go#L350-L366) | `{}` |
| `capm3.networkData.primary_pool_interface` | [CAPM3 NetworkDataRoutev4 route format](https://github.com/metal3-io/cluster-api-provider-metal3/blob/9d9e9404d835cb7dabdd0a3bbab79ef12c8f36cf/api/v1beta1/metal3datatemplate_types.go#L350-L366) | `{}` |
| `baremetal_host_default.credentials.username` | BMC inteface username | `""` |
| `baremetal_host_default.credentials.password` | BMC inteface password | `""` |
| `baremetal_host_default.bmh_metadata.labels` |  | `{}` |
| `baremetal_host_default.bmh_metadata.annotations` |  | `{}` |
| `baremetal_host_default.bmh_spec` | [BMO BMH spec](https://github.com/metal3-io/baremetal-operator/blob/main/docs/api.md#baremetalhost-spec) | `{}` |
| `baremetal_hosts` | set of BareMetalHosts | `{}` |
| `baremetal_hosts.x.automatedCleaningMode` | string, value "disabled" means don’t perform automated cleaning, while "metadata" enables automated cleaning. By default, we don't set this attribute in BareMetalHosts objects, so disks will be cleaned on first provisionning (it's the default behavior). We disable it in order to be able to reuse nodes storage on rolling updates (A specific Kyverno policiy will disable this parameter to BareMetalHosts once they are provisionned) | `{}` |
| `baremetal_hosts.x.ip_preallocations` | dict containing predictable IP allocation for BM nodes, see [CAPM3 node IP reuse](https://github.com/metal3-io/cluster-api-provider-metal3/blob/main/docs/ip_reuse.md#ip-reuse) | `{}` |
| `baremetal_hosts.x.ip_preallocations.provisioning` | IP allocation for BM node x on provisioning network  | `"172.20.39.214"` |
| `baremetal_hosts.x.ip_preallocations.primary` | IP allocation for BM node x on primary network  | `"172.20.36.142"` |
| `baremetal_hosts.x.interface_mappings` | dict containing optional interfaces re-mappings for BM nodes | `{}` |
| `baremetal_hosts.x.interface_mappings.if.mac_address` | Mac address of the physical interface "if" defined in network_interfaces for BM node x | `"52:54:00:01:00:01"` |
| `baremetal_hosts.x.interface_mappings.if.inspection_name` | Inspection name of the physical interface "if" defined in network_interfaces for BM node x | `"ens1f1"` |
| `kubelet_extra_args` | dict of kubelet args | `anonymous-auth: "false"` |
| `rke2.additionalUserData` | mapped to `RKE2ControlPlane.spec.agentConfig.additionalUserData` & `RKE2ConfigTemplate.spec.template.spec.agentConfig.additionalUserData` (with `.config` being a dictionary) | `{}` |
| `rke2.nodeLabels` | dict specifying labels for CP & MD nodes | `{}` |
| `rke2.nodeAnnotations` | dict specifying annotations for CP & MD nodes | `{}` |
{:/comment}
| `kubeadm` | mapped to `KubeadmControlPlane.spec.kubeadmConfigSpec` & `KubeadmConfigTemplate.spec.template.spec` | `{}` |
| `additional_commands.pre_bootstrap_commands` | set additional preKubeadmCommands (for cabpk bootstrap_provider) or preRKE2Commands (for cabpr bootstrap_provider) | `[]` |
| `additional_commands.post_bootstrap_commands` | set additional postKubeadmCommands (for cabpk bootstrap_provider) or postRKE2Commands (for cabpr bootstrap_provider) | `[]` |
| `additional_files` | dict of files to be injected as CAPI Machine file
| `additional_files.x.content` | string to be used as content of the CAPI Machine file to be loaded; if `encoding` is set, we pass `encoding` and content as-is, else we base-64 encode the contents | `""` |
| `additional_files.x.content_file` | the path of a file with the actual content of the CAPI Machine file to be loaded; if `encoding` is set, we pass `encoding` and content as-is, else we base-64 encode the contents; only used if `content` is not provided | `""` |
| `additional_files.x.content_k8s_secret` | a referenced source of content in the form of K8s Secret (in `.Release.Namespace` ns) to populate the CAPI Machine file; only used if neither `content` nor `content_file` is provided | `{}` |
| `additional_files.x.content_k8s_secret.key` | the key in the secret's data map for this value | `""` |
| `additional_files.x.content_k8s_secret.name` | name of the K8s secret (in the chart release namespace) to use | `""` |
| `additional_files.x.encoding` | specifies the encoding of the provided file contents ("base64", "gzip" or "zip+base64"); skip if plain-text | `""` |
| `additional_files.x.owner` | the ownership of the CAPI Machine file, e.g. "root:root" | `""` |
| `additional_files.x.path` | (optional, defaults additional_files entry x) the full path on disk where to store the CAPI Machine file | `""` |
| `additional_files.x.permissions` | the permissions to assign to the CAPI Machine file, e.g. "0640" | `""` |
| `audit_policies.omitStages` | OmitStages is a list of stages for which no events are created. Note that this can also be specified per rule in which case the union of both are omitted. | `[]` |
| `audit_policies.rules` | set of Rules. Rules specify the audit Level a request should be recorded at. A request may match multiple rules, in which case the FIRST matching rule is used. The default audit level is None, but can be overridden by a catch-all rule at the end of the list. PolicyRules are strictly ordered. | `[]` |
| `audit_policies.rules[].level` | The Level that requests matching this rule are recorded at.
| `audit_policies.rules[].users` | The users (by authenticated user name) this rule applies to. An empty list implies every user. | `[]` |
| `audit_policies.rules[].userGroups` |The user groups this rule applies to. A user is considered matching if it is a member of any of the UserGroups. An empty list implies every user group. | `[]` |
| `audit_policies.rules[].namespaces` | Namespaces that this rule matches. The empty string "" matches non-namespaced resources. An empty list implies every namespace. | "" |
| `audit_policies.rules[].omitStages` | OmitStages is a list of stages for which no events are created. Note that this can also be specified policy wide in which case the union of both are omitted. An empty list means no restrictions will apply. | |
| `audit_policies.rules[].nonResourceURLs` | NonResourceURLs is a set of URL paths that should be audited. *s are allowed, but only as the full, final step in the path | |
| `audit_policies.rules[].verbs` | The verbs that match this rule  ("get", "list", "delete", "watch", "create", "patch"  or "update"). An empty list implies every verb. | `[]` |
| `audit_policies.rules[].omitManagedFields` | OmitManagedFields indicates whether to omit the managed fields of the request and response bodies from being written to the API audit log. |  |
| `audit_policies.rules[].resources` | a set of Resources. Resources that this rule matches. An empty list implies all kinds in all API groups. | `[]` |
| `audit_policies.rules[].resources[].group` | Group is the name of the API group that contains the resources. The empty string represents the core API group. | `[]` |
| `audit_policies.rules[].resources[].resources` | Resources is a list of resources this rule applies to. | `[]` |
| `audit_policies.rules[].resources[].resources_name` | ResourceNames is a list of resource instance names that the policy matches. Using this field. requires Resources to be specified. An empty list implies that every instance of the resource is matched. | `[]` |
| `control_plane.kubelet_extra_args` |  | `anonymous-auth: "false"` |
| `control_plane.rolloutStrategy` | dict mapped to (kubeadmcontrolplane|rke2controlplane).spec.rolloutStrategy | defaults to rollingUpdate with maxSurge 1 (maxSurge 0 for capm3/baremetal) |
| `control_plane.rke2.additionalUserData` | mapped to `RKE2ControlPlane.spec.agentConfig.additionalUserData` (with `.config` being a dictionary) | `{}` |
| `control_plane.rke2.nodeLabels` | dict specifying labels for CP nodes | `{}` |
| `control_plane.rke2.nodeAnnotations` | dict specifying annotations for CP nodes | `{}` |
| `control_plane.kubeadm` | mapped to `KubeadmControlPlane.spec.kubeadmConfigSpec` | `{}` |
| `control_plane.additional_commands.pre_bootstrap_commands` | set additional preKubeadmCommands (for cabpk bootstrap_provider) or preRKE2Commands (for cabpr bootstrap_provider) | `[]` |
| `control_plane.additional_commands.post_bootstrap_commands` | set additional postKubeadmCommands (for cabpk bootstrap_provider) or postRKE2Commands (for cabpr bootstrap_provider) | `[]` |
| `control_plane.additional_files` | dict of files to be injected as CAPI CP Machine file
| `control_plane.additional_files.x.content` | string to be used as content of the CAPI CP Machine file to be loaded; if `encoding` is set, we pass `encoding` and content as-is, else we base-64 encode the contents | `""` |
| `control_plane.additional_files.x.content_file` | the path of a file with the actual content of the CAPI CP Machine file to be loaded; if `encoding` is set, we pass `encoding` and content as-is, else we base-64 encode the contents; only used if `content` is not provided | `""` |
| `control_plane.additional_files.x.content_k8s_secret` | a referenced source of content in the form of K8s Secret (in `.Release.Namespace` ns) to populate the CAPI CP Machine file; only used if neither `content` nor `content_file` is provided | `{}` |
| `control_plane.additional_files.x.content_k8s_secret.key` | the key in the secret's data map for this value | `""` |
| `control_plane.additional_files.x.content_k8s_secret.name` | name of the K8s secret (in the chart release namespace) to use | `""` |
| `control_plane.additional_files.x.encoding` | specifies the encoding of the provided file contents ("base64", "gzip" or "zip+base64"); skip if plain-text | `""` |
| `control_plane.additional_files.x.owner` | the ownership of the CAPI CP Machine file, e.g. "root:root" | `""` |
| `control_plane.additional_files.x.path` | (optional, defaults additional_files entry x) the full path on disk where to store the CAPI CP Machine file | `""` |
| `control_plane.additional_files.x.permissions` | the permissions to assign to the CAPI CP Machine file, e.g. "0640" | `""` |
| `control_plane.capo.os_image_selector` | OS image selector for CP nodes (dict)<br>see [OS image selection](#os-image-selection) | {} |
| `control_plane.capo.image_key` | key of image used for CP nodes (string)<br>see [OS image selection](#os-image-selection) |  |
| `control_plane.capo.image_name` | name of image used for CP nodes (string)<br>see [OS image selection](#os-image-selection) |  |
| `control_plane.capo.flavor_name` |  | `"m1.large"` |
| `control_plane.capo.rootVolume` | (optional) Let this parameter empty if you don't intend to use root volume for CAPO CP nodes | `{}` |
| `control_plane.capo.server_group_id` | generated by heat-operator in sylva | `"replace_me"` |
| `control_plane.capo.security_group_names` | OpenStack SG for control plane nodes | `[]` |
| `control_plane.capo.additional_security_group_names` | additional OpenStack SG for control plane nodes | `[]` |
| `control_plane.capv.diskGiB` | disk size for CP vSphereMachineTemplate | `50` |
| `control_plane.capv.memoryMiB` | memory size for CP vSphereMachineTemplate | `8192` |
| `control_plane.capv.numCPUs` | CPUs for CP vSphereMachineTemplate | `4` |
| `control_plane.capm3.os_image_selector` | OS image selector for CAPO CP nodes (dict)<br>see [OS image selection](#os-image-selection) | {} |
| `control_plane.capm3.image_key` | key of image used for CAPO CP nodes (string)<br>see [OS image selection](#os-image-selection) |  |
| `control_plane.capm3.machine_image_url` | URL for BM CP node image on a webserver<br>see [OS image selection](#os-image-selection) | `"http://55.55.55.55/ubuntu-22.04-plain.qcow2"` |
| `control_plane.capm3.machine_image_format` | format for BM CP node image<br>see [OS image selection](#os-image-selection) |  |
| `control_plane.capm3.machine_image_checksum` | checksum for BM CP node image, hosted on a webserver<br>see [OS image selection](#os-image-selection) |  |
| `control_plane.capm3.machine_image_checksum_type` | checksum type for BM CP node image<br>see [OS image selection](#os-image-selection) |  |
| `control_plane.capm3.hostSelector.matchLabels.cluster-role` |  | `"control-plane"` |
| `control_plane.capm3.nodeReuse` | boolean conditioning CAPM3 Machine controller to pick the same pool of CP BMHs' that were released during the upgrade operation, see [Metal3MachineTemplate.spec.nodeReuse](https://github.com/metal3-io/metal3-docs/blob/main/design/cluster-api-provider-metal3/node_reuse.md#node-reuse) | `true` |
| `control_plane.capm3.networkData.provisioning_pool_interface` | [CAPM3 NetworkDataRoutev4 route format](https://github.com/metal3-io/cluster-api-provider-metal3/blob/9d9e9404d835cb7dabdd0a3bbab79ef12c8f36cf/api/v1beta1/metal3datatemplate_types.go#L350-L366) | `{}` |
| `control_plane.capm3.networkData.primary_pool_interface` | [CAPM3 NetworkDataRoutev4 route format](https://github.com/metal3-io/cluster-api-provider-metal3/blob/9d9e9404d835cb7dabdd0a3bbab79ef12c8f36cf/api/v1beta1/metal3datatemplate_types.go#L350-L366) | `{}` |
| `control_plane.capm3.provisioning_pool_interface` |  | `"bond0"` |
| `control_plane.capm3.primary_pool_interface` |  | `""` |
| `control_plane.network_interfaces` |  | `{}` |
| `machine_deployment_default.replicas` | default number of worker nodes for each MachineDeployment | `0` |
| `machine_deployment_default.metadata.labels` |  | `{}` |
| `machine_deployment_default.metadata.annotations` |  | `{}` |
| `machine_deployment_default.kubelet_extra_args` |  | `anonymous-auth: "false"` |
| `machine_deployment_default.rke2.additionalUserData` | mapped to `RKE2ConfigTemplate.spec.template.spec.agentConfig.additionalUserData` (with `.config` being a dictionary) | `{}` |
| `machine_deployment_default.rke2.nodeLabels` | dict specifying labels for MD nodes | `{}` |
| `machine_deployment_default.rke2.nodeAnnotations` | dict specifying annotations for MD nodes | `{}` |
| `machine_deployment_default.kubeadm` | mapped to `KubeadmConfigTemplate.spec.template.spec` | `{}` |
| `machine_deployment_default.additional_commands.pre_bootstrap_commands` | set additional preKubeadmCommands (for cabpk bootstrap_provider) or preRKE2Commands (for cabpr bootstrap_provider) | `[]` |
| `machine_deployment_default.additional_commands.post_bootstrap_commands` | set additional postKubeadmCommands (for cabpk bootstrap_provider) or postRKE2Commands (for cabpr bootstrap_provider) | `[]` |
| `machine_deployment_default.additional_files` | dict of files to be injected as CAPI MD Machine file
| `machine_deployment_default.additional_files.x.content` | string to be used as content of the CAPI MD Machine file to be loaded; if `encoding` is set, we pass `encoding` and content as-is, else we base-64 encode the contents | `""` |
| `machine_deployment_default.additional_files.x.content_file` | the path of a file with the actual content of the CAPI MD Machine file to be loaded; if `encoding` is set, we pass `encoding` and content as-is, else we base-64 encode the contents; only used if `content` is not provided | `""` |
| `machine_deployment_default.additional_files.x.content_k8s_secret` | a referenced source of content in the form of K8s Secret (in `.Release.Namespace` ns) to populate the CAPI MD Machine file; only used if neither `content` nor `content_file` is provided | `{}` |
| `machine_deployment_default.additional_files.x.content_k8s_secret.key` | the key in the secret's data map for this value | `""` |
| `machine_deployment_default.additional_files.x.content_k8s_secret.name` | name of the K8s secret (in the chart release namespace) to use | `""` |
| `machine_deployment_default.additional_files.x.encoding` | specifies the encoding of the provided file contents ("base64", "gzip" or "zip+base64"); skip if plain-text | `""` |
| `machine_deployment_default.additional_files.x.owner` | the ownership of the CAPI MD Machine file, e.g. "root:root" | `""` |
| `machine_deployment_default.additional_files.x.path` | (optional, defaults additional_files entry x) the full path on disk where to store the CAPI MD Machine file | `""` |
| `machine_deployment_default.additional_files.x.permissions` | the permissions to assign to the CAPI MD Machine file, e.g. "0640" | `""` |
| `machine_deployment_default.capo.os_image_selector` | OS image selector for MD nodes (dict)<br>see [OS image selection](#os-image-selection) | {} |
| `machine_deployment_default.capo.image_key` | key of image used for MD nodes (string)<br>see [OS image selection](#os-image-selection) |  |
| `machine_deployment_default.capo.image_name` | name of image used for MD nodes (string)<br>see [OS image selection](#os-image-selection) |  |
| `machine_deployment_default.capo.flavor_name` |  | `"m1.large"` |
| `machine_deployment_default.capo.server_group_id` | generated by heat-operator in sylva | `"replace_me"` |
| `machine_deployment_default.capo.security_group_names` | OpenStack SG for worker nodes | `[]` |
| `machine_deployment_default.capo.additional_security_group_names` | additional OpenStack SG for worker nodes | `[]` |
| `machine_deployment_default.capo.rootVolume` | (optional) Let this parameter empty if you don't intend to use root volume for CAPO MD nodes | `{}` |
| `machine_deployment_default.capo.network_id` | (optional) OpenStack VN to attach MD nodes to, defaults to `.capo.network_id` | `"replace_me"` |
| `machine_deployment_default.capo.failure_domain` | (optional) OpenStack AZ for CAPO MD nodes | `"replace_me"` |
| `machine_deployment_default.capo.identity_ref_secret.clouds_yaml` | (optional) OpenStack clouds.yaml content for CAPO MD nodes, defaults to `.capo.clouds_yaml` if not provided | `"replace_me"` |
| `machine_deployment_default.capo.identity_ref_secret.clouds_yaml` | (optional) OpenStack clouds.yaml Secret name for CAPO MD nodes | `"replace_me"` |
| `machine_deployment_default.capv.image_name` |  | `""` |
| `machine_deployment_default.capv.diskGiB` | disk size for MD vSphereMachineTemplate | `50` |
| `machine_deployment_default.capv.memoryMiB` | memory size for MD vSphereMachineTemplate | `8192` |
| `machine_deployment_default.capv.numCPUs` | CPUs for MD vSphereMachineTemplate | `4` |
| `machine_deployment_default.capm3.os_image_selector` | OS image selector for CAPO MD nodes (dict)<br>see [OS image selection](#os-image-selection) | {} |
| `machine_deployment_default.capm3.image_key` | key of image used for CAPO MD nodes (string)<br>see [OS image selection](#os-image-selection) |  |
| `machine_deployment_default.capm3.machine_image_url` | URL for BM MD node image on a webserver<br>see [OS image selection](#os-image-selection) | `"http://55.55.55.55/ubuntu-22.04-plain.qcow2"` |
| `machine_deployment_default.capm3.machine_image_format` | format for BM MD node image<br>see [OS image selection](#os-image-selection) |  |
| `machine_deployment_default.capm3.machine_image_checksum` | checksum for BM MD node image, hosted on a webserver<br>see [OS image selection](#os-image-selection) |  |
| `machine_deployment_default.capm3.machine_image_checksum_type` | checksum type for BM MD node image<br>see [OS image selection](#os-image-selection) |  |
| `machine_deployment_default.capm3.hostSelector.matchLabels.cluster-role` |  | `"worker"` |
| `machine_deployment_default.capm3.nodeReuse` | boolean conditioning CAPM3 Machine controller to pick the same pool of MD BMHs' that were released during the upgrade operation, see [Metal3MachineTemplate.spec.nodeReuse](https://github.com/metal3-io/metal3-docs/blob/main/design/cluster-api-provider-metal3/node_reuse.md#node-reuse) | `true` |
| `machine_deployment_default.capm3.networkData.provisioning_pool_interface` | [CAPM3 NetworkDataRoutev4 route format](https://github.com/metal3-io/cluster-api-provider-metal3/blob/9d9e9404d835cb7dabdd0a3bbab79ef12c8f36cf/api/v1beta1/metal3datatemplate_types.go#L350-L366) | `{}` |
| `machine_deployment_default.capm3.networkData.primary_pool_interface` | [CAPM3 NetworkDataRoutev4 route format](https://github.com/metal3-io/cluster-api-provider-metal3/blob/9d9e9404d835cb7dabdd0a3bbab79ef12c8f36cf/api/v1beta1/metal3datatemplate_types.go#L350-L366) | `{}` |
| `machine_deployment_default.capm3.provisioning_pool_interface` |  | `"bond0"` |
| `machine_deployment_default.capm3.primary_pool_interface` |  | `""` |
| `machine_deployment_default.network_interfaces` | set of default values for MachineDeployments network interfaces | `{}` |
| `machine_deployment_default.machine_deployment_spec` | set of default fields for MachineDeployment.spec | `{}` |
| `machine_deployments` | set of MachineDeployments | `{}` |
| `machine_deployments.x.machine_deployment_spec` | set of fields for MachineDeployment.X.spec  | `{}` |
| `images.kube_vip.repository` |  | `"ghcr.io/kube-vip/kube-vip"` |
| `images.kube_vip.tag` |  | `"v0.5.12"` |
| `ntp` | Let this parameter empty if you don't intend to configure NTP on machines | `{}` |
| `proxies.https_proxy` |  | `""` |
| `proxies.http_proxy` |  | `""` |
| `proxies.no_proxy` |  | `""` |
| `registry_mirrors` | add your local registry mirror to avoid rate limiting | `{}` |
| `kube_vip.bgp_lbs` | add your kube-vip config if required | `{}` |
| `metallb.bgp_lbs` | add your metallb-l3 config if required | `{}` |
| `metallb.l2_lbs` | add your metallb-l2 additional config if required | `{}` |
| `calico_autodetection_method` | specify [calico autodetection method](https://docs.tigera.io/calico/latest/networking/ipam/ip-autodetection#change-the-autodetection-method) to bind interface | `{}` |
| `prevent_deletion` | prevents deletion of the Helm chart by having a pre-delete Helm hook Job designed to always fail and add specific annotations ( on Cluster and RKE2ControlPlan/KubeadmControlPlane resources) which avoid removal during Helm chart updates  | `false` |
| `openshift.ingressVIP` | ingress VIP for applications (required for OpenShift multi-controller cluster) | `""` |
| `openshift.version` |  | `"4.17.0"` |
| `openshift.releaseImage` | OpenShift release image URL | `"quay.io/okd/scos-release:4.17.0-0.okd-scos-2024-09-24-104828"` |
| `openshift.baseDomain` |  | `"sylva"` |
| `openshift.sshAuthorizedKey` | sshAuthorizedKey for OpenShift node user core | `""` |
| `openshift.pullSecret` |  | a dummy secret for OKD |
| `agent_config_format` | cloud-config or ignition | `"cloud-config"` |
---
_Documentation generated by [Frigate](https://frigate.readthedocs.io)._

### Details on `network_interfaces`

Nodes network interfaces are described in:

- `control_plane.network_interfaces`: interface definition for control plane nodes
- `machine_deployment_default.network_interfaces`: default interface definition for worker nodes
- `machine_deployments.xx.network_interfaces`: interface definition for given 'xx' MD worker nodes

Definition format differs according to target infra provider:

<details markdown=1>
<summary> For CAPO: </summary>

`network_interfaces` are mapped to `OpenStackMachineTemplate.spec.template.spec.ports`, additionally to the primary network port created for the OpenStack Network defined by `.capo.network_id`. Supported values are available using `kubectl explain OpenStackMachineTemplate.spec.template.spec.ports`

</details>
<details markdown=1>
<summary> For CAPV: </summary>

`network_interfaces` are mapped to `VSphereMachineTemplate.spec.template.spec.network.devices`, additionally to the network ports created for the vSphere Networks defined by `.capv.networks`.

| Parameter                | Description             |
| ------------------------ | ----------------------- |
| `<if_name>.networkName` | Network connected to interface <if_name> |
| `<if_name>.dhcp4` | boolean - Enable/Disable DHCP for interface <if_name> (default: false) |

</details>
<details markdown=1>
<summary> For CAPM3: </summary>

For most systems, `network_interfaces` is mapped to `Metal3Data.spec.template.spec.networkData.links`; the exception is OpenShift cluster, where `network_interfaces` is mapped to NMStateConfig interfaces.

| Parameter                | Description             |
| ------------------------ | ----------------------- |
| `<bond_name>.bond_mode` | Bond mode for <bond_name> (out of `balance-rr`, `active-backup`, `balance-xor`, `broadcast`, `balance-tlb`, `balance-alb` or `802.3ad`, defaults to `.capm3.network_interfaces.bond_mode`) |
| `<bond_name>.bondXmitHashPolicy` | Selects the transmit hash policy used for port selection in balance-xor and 802.3ad modes (out of `layer2`, `layer2+3` or `layer3+4`, defaults to `.capm3.network_interfaces.bondXmitHashPolicy`) |
| `<bond_name>.interfaces` | list - Interfaces included in <bond_name>. These interfaces must be defined in same stanza |
| `<bond_name>.vlans` | list - VLAN members to be handled by the <bond_name> interface. A logical interface <bond_name>.<id> will be created for each VLAN |
| `<if_name>.type` | Interface type (default: phy) |
| `<if_name>.vlans` | list - VLAN members to be handled by the <if_name> interface. A logical interface `<if_name>.<id>` will be created for each VLAN |

Example:

```yaml
network_interfaces:
  bond0:
    type: bond
    # bond_mode can be one of balance-rr, active-backup, balance-xor, broadcast, balance-tlb, balance-alb, 802.3ad
    bond_mode: balance-tlb
    interfaces:
      - ens1f0
      - ens1f1
    vlans:
      - id: 92
  bond1:
    type: bond
    bond_mode: 802.3ad
    # bondXmitHashPolicy can be one of layer2, layer2+3, layer3+4 when bond_mode is balance-xor or 802.3ad
    bondXmitHashPolicy: layer3+4
    interfaces:
      - ens2f0
      - ens2f1
    vlans:
      - id: 206
  ens1f0:
    vlans:
      - id: 92
  ens1f1:
    vlans:
      - id: 92
  ens2f0:
    type: phy
    vlans:
      - id: 206
  ens2f1:
    type: phy
    vlans:
      - id: 206
```

</details>

## Developing in this chart

As a developer in Sylva, or as a developer in a team using `sylva-capi-cluster` and wanting to add new functionality additionally to existing ones, you need to consider that this chart tries to handle properly the lifecycle of [Infrastructure Machine Templates](https://cluster-api.sigs.k8s.io/tasks/updating-machine-templates.html#updating-infrastructure-machine-templates) and [Bootstrap Templates](https://cluster-api.sigs.k8s.io/tasks/updating-machine-templates.html#updating-bootstrap-templates) (or [metaData and networkData](https://github.com/metal3-io/cluster-api-provider-metal3/blob/main/docs/api.md#updating-metadata-and-networkdata) in the particular case of [`Metal3`](https://github.com/metal3-io/cluster-api-provider-metal3)), by creating a new resource (achieved by appending a 10 digit subset from the hash of its immutable spec content) in order to ensure that a new object will be created whenever one of its (immutable) parameters is changed. <br/>
For this reason, the chart structure does come with a certain level of complexity, all the CAPI resources having specs with immutable fields being available as named templates (to handle the lifecycle of resource). The following diagrams are meant to assist in understanding this structure. <br/>

![sylva-capi-cluster chart cp](docs/img/sylva-capi-cluster-cp.drawio.png)
![sylva-capi-cluster chart md](docs/img/sylva-capi-cluster-md.drawio.png)

### Library dependency

This chart depends on [Sylva library](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-library) where some Sylva generic named templates are maintained.

To be able to run `helm template` command locally, you need to update dependencies with:

```
helm dependencies update .
```

### The values override structure

When writing this chart we had in mind the ability to override at multiple levels and thus we ended up with the following types of values:

- Global parameters that apply at cluster level.

Examples of these are `.name`, `.k8s_version`, `.control_plane_replicas`, all the infrastructure providers (`.capd`, `.capo`, `.capv`, `.capm3`), `.baremetal_hosts` and so on.

- Global parameters that can be overriden for a MachineDeployment (under `.machine_deployment_default` or `.machine_deployments.X`) mainly for usecases as specifying an infrastructure provider for an MD which could be different than CP nodes' one (for background, please see [sylva-core/-/issues/322](https://gitlab.com/sylva-projects/sylva-core/-/issues/322)) or installing worker nodes over multiple OpenStack platforms (for background, please see [sylva-core/-/issues/276](https://gitlab.com/sylva-projects/sylva-core/-/issues/276)) or modifying any fields of  MachineDeployments on machine_deployment_spec (for background, please see [https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster/-/issues/79](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster/-/issues/79)). Such examples would be: <br/>

> - `.capi_providers.infra_provider` by `.machine_deployments.X.infra_provider`, <br/>
> - `.capo.clouds_yaml` by `.machine_deployments.X.capo.identity_ref_secret.clouds_yaml` <br/>
> - `.capo.network_id` by `.machine_deployments.X.capo.network_id`. <br/>
> - or `.strategy.rollingUpdate.maxUnavailable` by `.machine_deployments.X.machine_deployment_spec.strategy.rollingUpdate.maxUnavailable`.

- Global parameters that apply to all machines (CP and MD) and which can get overriden for each role.

Such values are `.kubelet_extra_args`, `.rke2`, `.kubeadm` and some specific keys of cluster level parameters (like `.capv.memoryMiB` for example) and can be overriden for the CP specific needs by their equivalent under `.control_plane` and for MD under  `.machine_deployment_default` or `.machine_deployments.X`, respectively. For these values following _machine_deployments > machine_deployment_default > default_ and _control_plane > default_ logic, we'd have for example:

```yaml

capo:
  flavor_name: m1.medium # default OpenStack flavor name for both CP & MD

control_plane:
  capo:
    flavor_name: m1.large # OpenStack flavor name for CP only

machine_deployment_default:
  capo:
    flavor_name: m2.small # OpenStack flavor name for all MDs

machine_deployments:
  md0:
    capo:
      flavor_name: m2.medium # OpenStack flavor name for specifc MD only

```

and the templates would compute the most meaningful one (i.e. the values in `.control_plane.capo.flavor_name` and `.machine_deployments.md0.capo.flavor_name` respectively, for the above example).

<details markdown=1><summary>
Code sample (click to expand)
</summary>

We make fair use of [pluck](https://helm.sh/docs/chart_template_guide/function_list/#pluck) & [mergeoverwrite](https://helm.sh/docs/chart_template_guide/function_list/#mergeoverwrite-mustmergeoverwrite) to implement the override logic.

```go

// templates/_openstackmachinetemplatespec-cp.tpl
flavor: {{ pluck "flavor_name" .Values.capo (.Values.control_plane.capo | default dict) | last }}

```

```go

// templates/_openstackmachinetemplatespec-md.tpl
flavor: {{ pluck "flavor_name" $envAll.Values.capo ($machine_capo_specs | default dict) | last }}

```

where `$machine_capo_specs` is `$machine_deployment_def.capo` and `$machine_deployment_def` is already a merge of `.machine_deployments.X` with `.machine_deployment_default`, per

```go

{{- range $machine_deployment_name, $machine_deployment_specs := $envAll.Values.machine_deployments }}

  {{/*********** Finalize the definition of the machine_deployments.X
  item by merging the machine_deployment_default */}}
  {{- $machine_deployment_def := dict -}}
  {{- $machine_deployment_def := deepCopy ($envAll.Values.machine_deployment_default | default dict) -}}
  {{- $machine_deployment_def := mergeOverwrite $machine_deployment_def $machine_deployment_specs -}}

```

block we have in

```shell

templates/workers.yaml
templates/rke2configtemplate.yaml
templates/kubeadmconfigtemplate.yaml
templates/_Xmachinetemplate-md.tpl

```

</details>

## OS image selection

Depending on the infra of the cluster, there are different mechanisms to choose the disk image
used to deploy the operating system of the Kubernetes nodes.

To allow using different images for CP nodes and nodes of each MD, OS image selection is available in three locations:

- base: `.<infra_provider>` (e.g. `.capo`)
- control plane: `control_plane.<infra_provider>` (e.g `control_plane.capo`)
- MD default: `.machine_deployment_default.<infra_provider>` (e.g. `machine_deployment_default.capo`)
- per-MD: `.machine_deployments.X.<infra_provider>` (e.g. `machine_deployments.xxxxx.capo`)

**This section is essentially for OpenStack and Baremetal clusters**, for which
this chart, along with units of sylva-core/sylva-units, provides a mostly automated way of
selecting images, allowing those images to be automatically pulled from Sylva (or third-party)
OCI artifact repositories, and then automatically selected and injected in CAPI resources.

For other infra providers, e.g. vSphere, no similar mechanism exists and users have to manually push
images to the adequate backend and fill in their values whatever is applicable to tell Sylva to use
them via `image_name`.

For CAPO and CAPM3, there are 3 ways to select an OS image:

- by specifying an OS image selector `os_image_selector` (the **recommended** way, see below)
- by specifying an OS image key (a key from the `os_images` dictionnary, or when see from sylva-units of the `sylva_diskimagebuilder_images` dictionary)
- the "legacy" way:

  - for CAPO, specifying the Glance image name with `image_name`
  - for CAPM3, specifying the image URL and other parameters (`machine_image_url` + `machine_image_format` + `machine_image_checksum` + `machine_image_checksum_type`)

To facilitate backwards compatibility `os_image_selector` and `image_key` can be mixed (as long as they specifiy the same image), including with inheritance between the different levels (base, control plane, MD default, per-MD).  However the legacy approach cannot be used at the same time as `os_image_selector`/`image_key`.

### OS image selector (CAPO and CAPM3)

An "**OS Image selector**" is a simple dictionary specifying criteria that an OS image need to meet
to be selected.

For instance `capo.os_image_selector` is a place where such a selector can be used,
which in this case will be used to select the OS image used for an OpenStack deployment.

```yaml
capo:
  os_image_selector: # << an OS image selector
    os: ubuntu
    hardened: false
```

The key of such an image selector are matched against the annotations of the OCI artifacts in which
OS images are distributed.

An OS image selector can use the following keys:

- `os` (e.g. `ubuntu`, `opensuse`)
- `os-release` (e.g. `jammy`, `15.6`)
- `flavor` (e.g. `plain`, `hardened`)
- `hardened` (`true` `false`)

The following keys could also be used; however since `sylva-capi-cluster` automatically sets them based on your other parameters, **you should not set them** (unless you know what you're doing):

- `k8s-flavor` (e.g. `kubeadm`, `rke2`)
- `k8s-version` (e.g. `1.30.7`, `1.29.11+rke2r1`)

Given a set of OS images, an OS image will match a selector only if there is a match **for all keys**.

(see Sylva documentation portal for more advanced uses)

### OS image selector inheritance

Inheritance is supported for the different levels: multiple levels can specify an `os_image_selector`, and the keys of higer levels are inherited by the lower layers.

For instance:

```yaml
cluster:
capo:
  os_image_selector: # <<<<< base OS image selector
    os: sles
    hardened: false

control_plane:
  capo:
    os_image_selector:
      hardened: true  # <<< override for CP nodes

machine_deployemnts:
  #...
  special-md:
    capo:
      os_image_selector:
        os: ubuntu         # <<< override for nodes of MD special-md
  # ...
```

Such a configuration would result in using:

- for CP nodes, `sles` as the `os` with `hardened: true`
- for nodes of MD special-md, `ubuntu` as the `os` with `hardened: false`
- for other nodes (nodes of any other MD): `sles` as the `os`, without hardening

### `image_key` (CAPO and CAPM3)

When `image_key` is used, it has to be a key of the `os_images` dictionnary.

Keeping in mind that in the typical case where this chart is used from sylva-units, the `os_images` dictionary
is base on a merge of sylva-units `sylva_diskimagebuilder_images` and `os_images` dictionaries.

### vSphere (CAPV)

For vSphere only `image_name` can be used.

Example:

```yaml

capv:
  image_name: "Ubuntu 20.04 Packer"

control_plane:
  capv:
    image_name: "Ubuntu image 2 CP nodes"

# machine_deployment_default:
#  capv:
#    image_name: "Ubuntu 22.04 Packer"  # not overridden

machine_deployments: # OpenStack/vSphere image name for specifc MD only (no MD for CAPD)
  md0:
    capv:
      image_name: "Ubuntu 22.04 Packer special for MD0"
```

### CAPD

For CAPD only `image_name` can be used.

```yaml
capd:
  image_name: registry.gitlab.com/sylva-projects/sylva-elements/container-images rke2-in-docker:v1-24-12-rke2r1
```
