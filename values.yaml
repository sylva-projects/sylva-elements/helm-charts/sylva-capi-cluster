# Default values for sylva-capi-cluster.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

name: workload-cluster # name of the CAPI cluster

# agent_config_format can be either ignition or cloud-config
agent_config_format: cloud-config

# resource_groups is used to select which resources are produced
# more than one can be specified
#
# - all: have all resources be generated
# - baremetal-hosts: capm3 BareMetalHosts resources
# - cluster-resource: the Cluster CAPI resource
# - base: everything except what is covered above by baremetal-hosts and cluster-resource
#      ie. infra and bootstrap/controlplane resources, and all machine deployments, including
#      the resources they needs (with the exception of BareMetalHosts resources)
resource_groups:
  - all
  # - baremetal-hosts
  # - cluster-resource
  # - base
  # - all

air_gapped: false # can be set to true to do an RKE2 deployment disconnected from the Internet

cis_profile: "" # CIS profile to be used. Curently supported only for rke2 clusters. "cis-1.23" for 1.25-1.28, "cis" for 1.25+. Using the generic "cis" profile will ensure that the cluster passes the CIS benchmark associated with the Kubernetes version that RKE2 is running

# capi_providers: (required)
#   infra_provider: capo # capd, capo, capm3 or capv
#   bootstrap_provider: cabpk # cabpr (RKE2) or cabpk (kubeadm)

k8s_version: v1.24.12 # kubernetes version to be used; add "+rke2r1" for cabpr (RKE2) clusters

control_plane_replicas: 3 # nodes number for control plane

cluster_services_cidrs: # network ranges (in CIDR notation) from which Services networks are allocated
  - 100.73.0.0/16
cluster_pods_cidrs: # network ranges (in CIDR notation) from which Pod networks are allocated
  - 100.72.0.0/16

cni:
  default_provider: calico
  calico:
    helm_values: {}

enable_longhorn: false

prevent_deletion: false
#cluster_virtual_ip: 10.122.22.151 # address for kube-api (and other services) exposure
cluster_public_ip: ""  # (optional) when set, the cluster apiServerFixedIP will be set to this address and CAPI will connect to the cluster via this address (typically used with CAPO when a Floating IP is bound to `cluster_virtual_ip`)
cluster_primary_interfaces: [] ## list, (only for capm3) will default to [control_plane.capm3.primary_pool_interface]

cluster_api_cert_extra_SANs: [] # list of extra subjectAltNames to be added to cluster api endpoint certificate, useful if you want to access the api through a NAT or reverse-proxy

#mgmt_cluster_ip: 11.11.11.11 # management cluster external IP

helm_oci_url: # OCI registry endpoints for charts used in RKE2 deployments; leave it unset/empty for non-OCI based deployments (with Internet access)
  metallb: ""
  calico-crd: ""
  calico: ""
helm_versions: # in practice this is overridden by sylva-core
  metallb: "0.13.9"
  calico-crd: "v3.27.002"
  calico: "v3.27.002"
helm_extra_ca_certs: # Additional CA certs to be trusted for pulling helm artifacts (one string with the content of a PEM format file)
  default: ""
  metallb: ""
  calico: ""

etcd:
  # default sylva etcd values, should avoid "panic: etcdserver: mvcc: database space exceeded"
  quota-backend-bytes: "4294967296"  # default is 2GB max recommended is 8GB  reference: https://etcd.io/blog/2023/how_to_debug_large_db_size_issue/
  auto-compaction-retention: 12h     # reference: https://etcd.io/docs/v3.4/op-guide/maintenance/#auto-compaction
  auto-compaction-mode: periodic

capd:
  docker_host_socket: /var/run/docker.sock # host path extra mount for CAPD controller
  image_name: "" # image used for CABPR (RKE2) CAPD CP & MD nodes. ex: registry.gitlab.com/sylva-projects/sylva-elements/container-images/rke2-in-docker:v1-27-10-rke2r1

os_images: {}

capo:
  # image_name: replace_me # image used for CAPO CP & MD nodes
  # image_key: replace_me # image used for CAPO CP & MD nodes (key of .Values.os_images)
  flavor_name: m1.large
  # rootVolume: {} # Let this parameter empty if you don't intend to use root volume for CAPO CP & MD nodes
  # otherwise, provide following values
  # diskSize: 20 # Size of the VMs root disk
  # volumeType: '__DEFAULT__' # Type of volume to be created
  #control_plane_az: replace_me # list of OpenStack availability zones to deploy control planes nodes to, otherwise all would be candidates
  # ssh_key_name: replace_me # OpenStack VM SSH key
  # network_id: replace_me # OpenStack network used for nodes and VIP port
  # clouds_yaml: # (this is a dict, not a YAML string)
  #   clouds:
  #     capo_cloud:
  #       auth:
  #         auth_url: https://replace_me # $OS_AUTH_URL
  #         user_domain_name: replace_me # $OS_USER_DOMAIN_NAME
  #         project_domain_name: replace_me # $OS_PROJECT_DOMAIN_NAME
  #         project_name: replace_me # $OS_PROJECT_DOMAIN_NAME
  #         username: replace_me # $OS_USERNAME
  #         password: replace_me # $OS_PASSWORD
  #       region_name: replace_me # $OS_REGION_NAME
  #       verify: false # boolean for server cert check
  # #cacert: # cert used to validate CA of OpenStack APIs
  # resources_tag: sylva-tag # tag set for OpenStack resources
  # extra:
  #   allowed_address_pairs: # list of pairs objects, with 2 attributes: ip (mandatory) and mac (optional)
  #     - ip: "192.168.128.149"  # example with a single IP/MAC pair
  #       mac: EF:EF:EA:34:A3:33 # example with MAC address (will be converted to lower case in helm template)
  #     - ip: "192.168.128.131"  # example with only a single IP
  #     - ip: "42.42.42.0/24"    # example with a CIDR/MAC pair
  #       mac: ab:cd:ef:01:02:03

capv:
  username: replace_me # vSphere username
  password: replace_me # vSphere password
  dataCenter: "*" # Datacenter to use
  image_name: replace_me # image used for CAPV CP & MD nodes
  diskGiB: 50 # disk size for CP & MD vSphereMachineTemplate
  memoryMiB: 8192  # memory size for CP & MD vSphereMachineTemplate
  numCPUs: 4 # CPUs for CP & MD vSphereMachineTemplate
  networks: # vSphere network for all VMs and CSI
    default:
      networkName: "replace_me"
      dhcp4: true
    # additional-common:
    #   networkName: "replace_me"
    #   dhcp4: true
  server: replace_me # vSphere server DNS name or IP
  #dataStore: replace_me # vSphere datastore name
  tlsThumbprint: replace_me # vSphere https TLS thumbprint
  ssh_key: replace_me # SSH public key for VM access
  folder: replace_me # vSphere folder
  resourcePool: replace_me # vSphere resourcepool
  template_clone_mode: "fullClone" # possible values: "fullClone", "linkedClone"
  storagePolicyName: "" # vSphere storage policy name

capm3:
  primary_pool_name: "primary-pool"
  # primary_pool_network: 10.122.22.128
  # primary_pool_gateway: 10.122.22.129
  # primary_pool_end: 10.122.22.150
  # primary_pool_start: 10.122.22.140
  # primary_pool_prefix: 26
  # provisioning_pool_name: "provisioning-pool"
  # provisioning_pool_network: 10.91.91.192
  # provisioning_pool_gateway: 10.91.91.193
  # provisioning_pool_end: 10.91.91.219
  # provisioning_pool_start: 10.91.91.213
  # provisioning_pool_prefix: 26
  dns_servers:
  - 1.1.1.1
  - 8.8.8.8
  use_os_image_server_service_urls: false # Set to true in the case of libvirt-metal
  # image_provisioning_host: .Values.mgmt_cluster_ip # The hostname/IP on which os-image-server is serving images for this cluster's hosts
  # os_image_selector: # see TBC link to doc on OS image selectors
  # image_key: replace_me # image used for CAPM3 CP & MD nodes (key of .Values.capm3.os_images)
  # machine_image_url: http://55.55.55.55/ubuntu-22.04-plain.qcow2 # URL for BM CP & MD node image on a webserver
  # machine_image_format: qcow2 # format for BM CP & MD node image
  # machine_image_checksum: http://55.55.55.55/ubuntu-22.04-plain.qcow2.sha256sum # checksum for BM CP & MD node image, hosted on a webserver
  # machine_image_checksum_type: md5 # checksum type for BM CP & MD node image
  nodeReuse: true # if true CAPM3 Machine controller will pick the same pool of BMHs' that were released during the upgrade operation
  #label_sync_prefixes: node-label,sylva.org # comma separated list of labels that should be copied from BareMetalHosts to nodes 
  networkData:
    provisioning_pool_interface: {}
    primary_pool_interface: {}
      # routes:
      # - network: "0.0.0.0"
      #   prefix: 0
      #   gateway:
      #     string: "192.168.1.254"
      #   services:
      #     dns:
      #     - "8.8.4.4"
  network_interfaces:
    bondXmitHashPolicy: "layer3+4"   # default value for Metal3DataTemplate.spec.networkData.links.bonds[*].bondXmitHashPolicy
    bond_mode: "802.3ad"   # default value for Metal3DataTemplate.spec.networkData.links.bonds[*].bondMode; out of balance-rr, active-backup, balance-xor, broadcast, balance-tlb, balance-alb, 802.3ad
    mtu: 1500
  provisioning_pool_interface: ""
#  primary_pool_interface: bond0.13

baremetal_host_default:
  bmh_metadata:
    labels:
      host-type: generic
#    credentials:
#      username: Administrator
#      password: admin
#    bmh_metadata:
#      labels:
#        cluster-role: control-plane
#    bmh_spec:
#      online: true
#      externallyProvisioned: false
#      bmc:
#        address: redfish-virtualmedia://
#        disableCertificateVerification: true
#      automatedCleaningMode: metadata
#      bootMACAddress:
#      bootMode: legacy
#      rootDeviceHints:
#        hctl:
#
baremetal_hosts: {}
#  bmh1:
#    credentials:
#      username: admin1
#      password: admin1
#    longhorn_disk_config:
#      - path: "/var/longhorn/disks/disk_by-path_pci-0000:18:00.0-scsi-0:3:111:0"
#        storageReserved: 0
#        allowScheduling: true
#        tags:
#          - ssd
#          - fast
#      - path: "/var/longhorn/disks/sde"
#        storageReserved: 0
#        allowScheduling: true
#        tags:
#          - hdd
#          - slow
#    bmh_metadata:
#      labels:
#        snmp-enabled: "true"                           # label used as Kyverno policy selector for computing Prometheus ConfigMap based on BMH annotations
#      annotations:
#        sylvaproject.org/snmp-auth: dell1              # key in sylva-units .Values.snmp.auth
#        sylvaproject.org/snmp-hw-type: dell_idrac      # key in sylva-snmp-resources .Values.modules; can be dell_idrac or hp_cpq
#        sylvaproject.org/snmp-endpoint: 10.10.10.10    # SNMP walk endpoint
#    bmh_spec:
#      description: Downstream dev clusters
#      bmc:
#        address: redfish-virtualmedia://172.20.188.242/redfish/v1/Systems/1
#      bootMACAddress: 48:df:37:e7:7a:30
#      rootDeviceHints:
#        hctl: 2:1:0:0
#    ip_preallocations:               # provide IP addresses to enable DHCP-less ironic-python-agent
#      primary: 10.122.22.140
#      provisioning: 10.91.91.213
#  bmh2:
#    credentials:
#      username: admin2
#      password: admin2
#    bmh_metadata:
#      labels:
#        cluster-role: worker
#    ip_preallocations:                     # provide IP addresses to enable DHCP-less ironic-python-agent
#      primary: 10.122.22.141
#      provisioning: 10.91.91.214
#    interface_mappings:                    # optionally redefine how network_interfaces should be mapped in this host
#      ens1f0:
#        mac_address: ab:cd:ef:01:02:03     # provide the mac-address of the interface that should be configured as ens1f0 (defined in network_interfaces)
#      ens1f1:
#        inspection_name: ens4              # provide the inspection name (as seen in BareMetalHost status) of the interface should be configured as ens1f1

kubelet_extra_args:
  anonymous-auth: "false"
  eviction-hard: "imagefs.available<5%,nodefs.available<5%,memory.available<1Gi"
rke2:
  additionalUserData:
    # strict: true # if true, warnings are treated as error when parsing config section
    config: {} # dict specifying additional cloud-init configuration
    #   users:
    #     - name: ubuntu
    #       groups: users
    #       sudo: ALL=(ALL) NOPASSWD:ALL
    #       shell: /bin/bash
    #       ssh_authorized_keys:
    #         - "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQC4\
    #            S9DxGu2ej3BbMmgbnQHMWyan2j5OW1V3DsR93XSu\
    #            neCNt13AXNTsqOtiEwUwv2Ii/rXPjYDJ4i1wJAHP\
    #            E0+x2PYVHw4Bnh78VV4ez/PbWO/FkztBVlfh4X+K\
    #            NbMi7DefaExqNdnm0A4pxz+H3EwBxZCPw+Ydz+IP\
    #            XPL6yIQySw=="
  nodeLabels: {}
  # sylva.org/label-scope: "test"
  nodeAnnotations: {}
kubeadm: {}
  # users:
  #   - name: ubuntu
  #     groups: users
  #     sudo: ALL=(ALL) NOPASSWD:ALL
  #     shell: /bin/bash
  #     sshAuthorizedKeys:
  #       - "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQC4\
  #           S9DxGu2ej3BbMmgbnQHMWyan2j5OW1V3DsR93XSu\
  #           neCNt13AXNTsqOtiEwUwv2Ii/rXPjYDJ4i1wJAHP\
  #           E0+x2PYVHw4Bnh78VV4ez/PbWO/FkztBVlfh4X+K\
  #           NbMi7DefaExqNdnm0A4pxz+H3EwBxZCPw+Ydz+IP\
  #           XPL6yIQySw=="

additional_commands: {}
#  pre_bootstrap_commands: [] # set additional preKubeadmCommands (for cabpk bootstrap_provider) or preRKE2Commands (for cabpr bootstrap_provider)
#  post_bootstrap_commands: [] # set additional postKubeadmCommands (for cabpk bootstrap_provider) or postRKE2Commands (for cabpr bootstrap_provider)

#audit_policies:
#  omitStages:  # Don't generate audit events for all requests in RequestReceived stage.
#   - "RequestReceived"
#  rules:
#  - level: Metadata # Log secrets and configmaps changes at metadata level
#    users:
#    - "system:kube-scheduler"
#    - "system:kube-proxy"
#    userGroups: ["system:authenticated"]
#    namespaces: []
#    omitStages:
#    - "RequestReceived"
#    nonResourceURLs:
#    - "/api*"
#    - "/version"
#    verbs: ["create","get","delete"]
#    omitManagedFields: false
#    resources:
#    - group: ""
#      resources: ["secrets", "configmaps"]
#      resources_name: ["secrets", "configmaps"]


additional_files: {}
  # my-custom-file:
  #   content:
  #     "Lorem ipsum dolor sit amet,\n
  #     consectetur adipiscing elit,\n
  #     sed do eiusmod tempor."
  #   content_file: "my-custom-file.toml"  # the path of a file with the actual content of the CAPI Machine file to be loaded; must be inside the chart (Helm .Files.Get not being able to access the file system arbitrarily is a security feature)
  #   content_k8s_secret: {}  # used only if content_file is not provided, a referenced source of content in the form of K8s Secret (in `.Release.Namespace` ns) to populate the CAPI Machine file
  #     # key: ""  # the key in the secret's data map for this value
  #     # name: ""  # name of the secret (in the chart release namespace) to use
  #   encoding: "base64"  # specifies the encoding of the `content_file` contents; skip if plain-text
  #   owner: "root:root"  # the ownership of the CAPI Machine file, e.g. "root:root"
  #   path: "/my/custom/file"  # the full path on disk where to store the CAPI Machine file
  #   permissions: "0640"  # the permissions to assign to the CAPI Machine file, e.g. "0640"

control_plane:
  # node_class: generic
  kubelet_extra_args:
    anonymous-auth: "false"
  #rolloutStrategy:  # maps to (kubeadmcontrolplane|rke2controlplane).spec.rolloutStrategy
    #type: RollingUpdate
    #rollingUpdate:
    #  maxSurge: # defaults to 1, except for capm3, where it defaults to 0
  rke2:
    additionalUserData:
      # strict: true # if true, warnings are treated as error when parsing config section
      config: {} # dict specifying additional cloud-init configuration
      #   users:
      #     - name: ubuntu
      #       groups: users
      #       sudo: ALL=(ALL) NOPASSWD:ALL
      #       shell: /bin/bash
      #       ssh_authorized_keys:
      #         - "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQC4\
      #            S9DxGu2ej3BbMmgbnQHMWyan2j5OW1V3DsR93XSu\
      #            neCNt13AXNTsqOtiEwUwv2Ii/rXPjYDJ4i1wJAHP\
      #            E0+x2PYVHw4Bnh78VV4ez/PbWO/FkztBVlfh4X+K\
      #            NbMi7DefaExqNdnm0A4pxz+H3EwBxZCPw+Ydz+IP\
      #            XPL6yIQySw=="
    # nodeLabels: {} # dict specifying labels for CP nodes
    # nodeAnnotations: {} # dict specifying annotations for CP nodes
  kubeadm:
    nodeRegistration:
      taints: []
  #   users:
  #     - name: ubuntu
  #       groups: users
  #       sudo: ALL=(ALL) NOPASSWD:ALL
  #       shell: /bin/bash
  #       ssh_authorized_keys:
  #         - "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQC4\
  #             S9DxGu2ej3BbMmgbnQHMWyan2j5OW1V3DsR93XSu\
  #             neCNt13AXNTsqOtiEwUwv2Ii/rXPjYDJ4i1wJAHP\
  #             E0+x2PYVHw4Bnh78VV4ez/PbWO/FkztBVlfh4X+K\
  #             NbMi7DefaExqNdnm0A4pxz+H3EwBxZCPw+Ydz+IP\
  #             XPL6yIQySw=="
  additional_commands:
    pre_bootstrap_commands: [] # set additional preKubeadmCommands (for cabpk bootstrap_provider) or preRKE2Commands (for cabpr bootstrap_provider)
    post_bootstrap_commands: [] # set additional postKubeadmCommands (for cabpk bootstrap_provider) or postRKE2Commands (for cabpr bootstrap_provider)
  additional_files: {} # set of additional file declarations for machines, where contents can be injected from an external file
  capd: {}
    # image_name: <replace_me> # image used for CAPD CP nodes
  capo: {}
    # os_image_selector: # see TBC link to doc on OS image selectors
    # image_key: replace_me # image used for CAPO CP & MD nodes (key of .Values.os_images)
    # image_name: replace_me # image used for CAPO CP nodes
    # flavor_name: m1.large
    # rootVolume:
    #   diskSize: 20 # Size of the VMs root disk
    #   volumeType: '__DEFAULT__' # Type of volume to be created
    # server_group_id: replace_me # generated by heat-operator in sylva
    # security_group_name: capo-cluster-security-group-ctrl-plane # OpenStack SG for control plane nodes
  capv: {}
    # image_name: replace_me # image used for CAPV CP nodes
    # diskGiB: 50 # disk size for vSphereMachineTemplate of CAPV CP nodes
    # memoryMiB: 8192  # memory size for vSphereMachineTemplate of CAPV CP nodes
    # numCPUs: 4 # CPUs for vSphereMachineTemplate
  capm3:
    # os_image_selector: # see TBC link to doc on OS image selectors
    # image_key: replace_me # image used for CAPM3 CP nodes (key of .Values.os_images)
    # machine_image_url: # URL for BM CP image on a webserver
    # machine_image_format: qcow2 # format for BM CP node image
    # machine_image_checksum: # checksum for BM CP node image, hosted on a webserver
    # machine_image_checksum_type: md5 # checksum type for BM CP node image
    # nodeReuse: true # if true CAPM3 Machine controller will pick the same pool of CP BMHs' that were released during the upgrade operation
    hostSelector:  # criteria for matching labels on BareMetalHost objects; can be used to limit the set of available BareMetalHost objects chosen for this Machine type; see `kubectl explain metal3machinetemplate.spec.template.spec.hostSelector`
      matchLabels:
        host-type: generic
    networkData:
      provisioning_pool_interface: {}
        # routes:
        # - network: "192.168.220.0"
        #   prefix: 24
        #   gateway:
        #     string: "192.168.120.254"
      primary_pool_interface: {}
        # routes:
        # - network: "0.0.0.0"
        #   prefix: 0
        #   gateway:
        #     string: "192.168.2.254"
    # provisioning_pool_interface: bond0
    # primary_pool_interface: bond0.13
  network_interfaces: {}
    # for CAPM3 folowing are used and mapped to Metal3Data.spec.template.spec.networkData.links
    # bond0:
    #   type: bond
    #   # bond_mode can be one of balance-rr, active-backup, balance-xor, broadcast, balance-tlb, balance-alb, 802.3ad
    #   # https://github.com/metal3-io/cluster-api-provider-metal3/blob/v1.6.0/api/v1beta1/metal3datatemplate_types.go#L245-L246
    #   # if missed, it defaults to .capm3.network_interfaces.bond_mode
    #   bond_mode: 802.3ad
    #   # bondXmitHashPolicy can be one of layer2, layer2+3, layer3+4 when bond_mode is balance-xor or 802.3ad
    #   # https://github.com/metal3-io/cluster-api-provider-metal3/blob/v1.6.0/api/v1beta1/metal3datatemplate_types.go#L249-L250
    #   # if missed, it defaults to .capm3.network_interfaces.bondXmitHashPolicy
    #   bondXmitHashPolicy: layer2+3
    #   interfaces:
    #     - ens1f0
    #     - ens1f1
    #   vlans:
    #     - id: 13
    # ens1f0:
    #   type: phy
    # ens1f1:
    #   type: phy
    #   macAddress:
    #     fromHostInterface: "eth1"   # optionally provide interface name from BareMetalHost hardware details to use MAC from
openshift:
  version: 4.17.0
  releaseImage: quay.io/okd/scos-release:4.17.0-0.okd-scos-2024-09-24-104828
  baseDomain: "sylva"
  sshAuthorizedKey: ""
  pullSecret: ""

machine_deployment_default: # set of default values for MachineDeployments
  replicas: 0 # default number of worker nodes for each MachineDeployment
  machine_deployment_spec: {}
    # strategy:
    #   rollingUpdate:
    #     maxUnavailable: 2
    #     maxSurge: 1
  # node_class: generic
  metadata:
    labels: {}
    annotations: {}
  kubelet_extra_args:
    anonymous-auth: "false"
  # infra_provider: capo # capd, capo, capm3 or capv
  rke2:
    additionalUserData:
      # strict: true # if true, warnings are treated as error when parsing config section
      config: {} # dict specifying additional cloud-init configuration
      #   users:
      #     - name: ubuntu
      #       groups: users
      #       sudo: ALL=(ALL) NOPASSWD:ALL
      #       shell: /bin/bash
      #       ssh_authorized_keys:
      #         - "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQC4\
      #            S9DxGu2ej3BbMmgbnQHMWyan2j5OW1V3DsR93XSu\
      #            neCNt13AXNTsqOtiEwUwv2Ii/rXPjYDJ4i1wJAHP\
      #            E0+x2PYVHw4Bnh78VV4ez/PbWO/FkztBVlfh4X+K\
      #            NbMi7DefaExqNdnm0A4pxz+H3EwBxZCPw+Ydz+IP\
      #            XPL6yIQySw=="
    # nodeLabels: {} # dict specifying labels for MD nodes
    # nodeAnnotations: {} # dict specifying annotations for MD nodes
  #kubeadm:
  #  users:
  #    - name: ubuntu
  #      groups: users
  #      sudo: ALL=(ALL) NOPASSWD:ALL
  #      shell: /bin/bash
  #      ssh_authorized_keys:
  #        - "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQC4\
  #            S9DxGu2ej3BbMmgbnQHMWyan2j5OW1V3DsR93XSu\
  #            neCNt13AXNTsqOtiEwUwv2Ii/rXPjYDJ4i1wJAHP\
  #            E0+x2PYVHw4Bnh78VV4ez/PbWO/FkztBVlfh4X+K\
  #            NbMi7DefaExqNdnm0A4pxz+H3EwBxZCPw+Ydz+IP\
  #            XPL6yIQySw=="
  additional_commands:
    pre_bootstrap_commands: [] # set additional preKubeadmCommands (for cabpk bootstrap_provider) or preRKE2Commands (for cabpr bootstrap_provider)
    post_bootstrap_commands: [] # set additional postKubeadmCommands (for cabpk bootstrap_provider) or postRKE2Commands (for cabpr bootstrap_provider)
  additional_files: {} # set of additional file declarations for machines, where contents can be injected from an external file
  capo: {}
    # network_id: 6bc87ec1-821b-4ee5-bd88-01151a45f3b3 # the OpenStack VN to attach MD nodes to, defaults to .capo.network_id
    # failure_domain: region0 # the AZ for CAPO MD nodes
    # flavor_name: m1.large
    # rootVolume:
    #   diskSize: 20 # Size of the VMs root disk
    #   volumeType: '__DEFAULT__' # Type of volume to be created
    # os_image_selector: # see TBC link to doc on OS image selectors
    # image_key: replace_me # image used for CAPO CP & MD nodes (key of .Values.os_images)
    # image_name: "" # image for all CAPO MD nodes
    # server_group_id: 7fb8d72f-f25c-4ada-ab49-9a4fba4b6d91 # the OpenStack server group ID where nodes belong to
    # security_group_name: 1876a1b1-488c-44ea-835c-055a7a17c0e7 # the OpenStack security group name assigned to nodes
  capv: {} # hardware profile for CAPV MD nodes
    # diskGiB: 50 # disk size for vSphereMachineTemplate
    # memoryMiB: 8192  # memory size for vSphereMachineTemplate
    # numCPUs: 4 # CPUs for vSphereMachineTemplate
    # image_name: "" # image for all CAPV MD nodes
  capm3:
    # os_image_selector: # see TBC link to doc on OS image selectors
    # image_key: replace_me # image used for CAPM3 MD nodes (key of .Values.os_images)
    # machine_image_url: # URL for BM MD node image on a webserver
    # machine_image_format: qcow2 # format for BM MD node image
    # machine_image_checksum: # checksum for BM MD node image, hosted on a webserver
    # machine_image_checksum_type: md5 # checksum type for BM MD node image
    # nodeReuse: true # if true CAPM3 Machine controller will pick the same pool of MD BMHs' that were released during the upgrade operation
    hostSelector:  # criteria for matching labels on BareMetalHost objects; can be used to limit the set of available BareMetalHost objects chosen for this Machine type; see `kubectl explain metal3machinetemplate.spec.template.spec.hostSelector`
      matchLabels:
        host-type: generic
    networkData:
      provisioning_pool_interface: {}
        # routes:
        # - network: "192.168.210.0"
        #   prefix: 24
        #   gateway:
        #     string: "192.168.110.254"
      primary_pool_interface: {}
        # routes:
        # - network: "0.0.0.0"
        #   prefix: 0
        #   gateway:
        #     string: "192.168.2.254"
    # provisioning_pool_interface: bond0
    # primary_pool_interface: bond0.13
  network_interfaces: {} # set of default values for MachineDeployments network interfaces

machine_deployments: {} # set of MachineDeployments
  # md0:
  #   infra_provider: capo # capd, capo, capm3 or capv; defaults to `capi_providers.infra_provider`
  #   replicas: 2
  #   node_class: generic
  #   metadata:
  #     labels:
  #       mdLabel: mdLabelValue0
  #   capm3:
  #     os_image_selector: # see TBC link to doc on OS image selectors
  #     image_key: replace_me # image used for this CAPM3 MD nodes (key of .Values.os_images)
  #     machine_image_url: # URL for BM MD node image on a webserver
  #     machine_image_format: qcow2 # format for BM MD node image
  #     machine_image_checksum: # checksum for BM MD node image, hosted on a webserver
  #     machine_image_checksum_type: md5 # checksum type for BM MD node image
  #     hostSelector:
  #       matchLabels:
  #         cluster-role: worker-intensive
  #      networkData:
  #        provisioning_pool_interface:
  #          routes:
  #          - network: "192.168.211.0"
  #            prefix: 24
  #            gateway:
  #              string: "192.168.111.254"
  #        primary_pool_interface:
  #          routes:
  #          - network: "0.0.0.0"
  #            prefix: 0
  #            gateway:
  #              string: "192.168.2.254"
  #   network_interfaces:
  #    # for CAPM3 folowing are used and mapped to Metal3Data.spec.template.spec.networkData.links
  #     bond0:
  #       type: bond
  #       # bond_mode can be one of balance-rr, active-backup, balance-xor, broadcast, balance-tlb, balance-alb, 802.3ad
  #       # https://github.com/metal3-io/cluster-api-provider-metal3/blob/v1.6.0/api/v1beta1/metal3datatemplate_types.go#L245-L246
  #       # if missed, it defaults to .capm3.network_interfaces.bond_mode
  #       bond_mode: balance-tlb   # no bondXmitHashPolicy would apply
  #       interfaces:
  #         - ens1f0
  #         - ens1f1
  #       vlans:
  #         - id: 92
  #     bond1:
  #       type: bond
  #       bond_mode: 802.3ad
  #       interfaces:
  #         - ens2f0
  #         - ens2f1
  #       vlans:
  #         - id: 206
  #     ens1f0:
  #       vlans:
  #         - id: 92
  #     ens1f1:
  #       vlans:
  #         - id: 92
  #     ens2f0:
  #       type: phy
  #       vlans:
  #         - id: 206
  #     ens2f1:
  #       type: phy
  #       vlans:
  #         - id: 206
  #     # for CAPO folowing are used and mapped to OpenStackMachine.spec.template.spec.ports, additionally to the primary network port
  #     secondary: # .description
  #       # for all options available see `kubectl explain OpenStackMachineTemplate.spec.template.spec.ports`
  #       network:
  #         id: c314d52c-80fe-42b6-9092-55be383d1951 # .network.id; VN needs to exist in OpenStack tenant
  #       vnicType: direct # .vnicType; can be "direct" or "normal"
  #     # for CAPV following are used and mapped to VSphereMachineTemplate.spec.template.spec.network.devices
  #     additional:
  #       networkName: "replace_me"
  #       dhcp4: true # this parameter, for each networkName, is default to false
  #   # for CAPV following are used to fill the corresponding values (i.e. numCPUs, memoryMiB and diskGiB) in VSphereMachineTemplate of corresponding md
  #   capv: # hardware profile for CAPV MD nodes
  #     numCPUs: 4
  #     memoryMiB: 8192
  #     diskGiB: 50
  #   rke2:
  #     additionalUserData:
  #       strict: true # if true, warnings are treated as error when parsing config section
  #       config: # dict specifying additional cloud-init configuration
  #         ssh_pwauth: True
  #         chpasswd:
  #           list: |
  #             ubuntu:secret
  #           expire: False
  #         bootcmd:
  #           - if [ ! -f "/var/lib/grub-init" ]; then
  #           - touch "/var/lib/grub-init"
  #           - current_grub=$(grep '^GRUB_CMDLINE_LINUX_DEFAULT=' /etc/default/grub | tail -1 | sed -e "s/^[^=]*=[\"']\\?//" -e "s/['\"]$//")
  #           - next_grub="$current_grub ${HUGE_PAGES}"
  #           - sed -i -e '/GRUB_TIMEOUT=5/ d' /etc/default/grub
  #           - sed -i -e '/^GRUB_CMDLINE_LINUX_DEFAULT=/ d' /etc/default/grub
  #           - echo "GRUB_RECORDFAIL_TIMEOUT=1" >> /etc/default/grub
  #           - echo "GRUB_CMDLINE_LINUX_DEFAULT=\"$next_grub\"" >> /etc/default/grub
  #           - update-grub
  #           - cloud-init clean --reboot
  #           - fi
  #         users:
  #           - name: ubuntu
  #             groups: users
  #             sudo: ALL=(ALL) NOPASSWD:ALL
  #             shell: /bin/bash
  #             ssh_authorized_keys:
  #               - "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQC4\
  #                  S9DxGu2ej3BbMmgbnQHMWyan2j5OW1V3DsR93XSu\
  #                  neCNt13AXNTsqOtiEwUwv2Ii/rXPjYDJ4i1wJAHP\
  #                  E0+x2PYVHw4Bnh78VV4ez/PbWO/FkztBVlfh4X+K\
  #                  NbMi7DefaExqNdnm0A4pxz+H3EwBxZCPw+Ydz+IP\
  #                  XPL6yIQySw=="
  #   kubeadm:
  #     users:
  #       - name: ubuntu
  #         groups: users
  #         sudo: ALL=(ALL) NOPASSWD:ALL
  #         shell: /bin/bash
  #         sshAuthorizedKeys:
  #           - "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQC4\
  #              S9DxGu2ej3BbMmgbnQHMWyan2j5OW1V3DsR93XSu\
  #              neCNt13AXNTsqOtiEwUwv2Ii/rXPjYDJ4i1wJAHP\
  #              E0+x2PYVHw4Bnh78VV4ez/PbWO/FkztBVlfh4X+K\
  #              NbMi7DefaExqNdnm0A4pxz+H3EwBxZCPw+Ydz+IP\
  #              XPL6yIQySw=="
  #   additional_commands:
  #     pre_bootstrap_commands:
  #     post_bootstrap_commands:
  # md1:
  #   # infra_provider: capo # capd, capo, capm3 or capv
  #   # replicas: 3
  #   node_class: generic
  #   metadata:
  #     annotations:
  #       mdAnnotation: mdAnnotationValue1
  #   capm3:
  #     hostSelector:
  #       matchLabels:
  #         cluster-role: worker-sriov
  #   network_interfaces:
  #     # for CAPM3 folowing are used and mapped to Metal3Data.spec.template.spec.networkData.links
  #     bond0:
  #       type: bond
  #       # bond_mode can be one of balance-rr, active-backup, balance-xor, broadcast, balance-tlb, balance-alb, 802.3ad
  #       # https://github.com/metal3-io/cluster-api-provider-metal3/blob/v1.6.0/api/v1beta1/metal3datatemplate_types.go#L245-L246
  #       # if missed, it defaults to .capm3.network_interfaces.bond_mode
  #       bond_mode: balance-xor
  #       # bondXmitHashPolicy can be one of layer2, layer2+3, layer3+4 when bond_mode is balance-xor or 802.3ad
  #       # https://github.com/metal3-io/cluster-api-provider-metal3/blob/v1.6.0/api/v1beta1/metal3datatemplate_types.go#L249-L250
  #       # if missed, it defaults to .capm3.network_interfaces.bondXmitHashPolicy
  #       bondXmitHashPolicy: layer3+4
  #       interfaces:
  #         - ens1f0
  #         - ens1f1
  #       vlans:
  #         - id: 33
  #     ens1f0:
  #       vlans:
  #         - id: 33
  #     ens1f1:
  #       vlans:
  #         - id: 33
  #   capo:
  #     flavor_name: m1.large
  #     failure_domain: region1
  #     identity_ref_secret:
  #       name: dr-site-capo-cloud-config
  #       clouds_yaml: # (this is a dict, not a YAML string)
  #         clouds:
  #           capo_cloud:
  #             auth:
  #               auth_url: https://replace_me # $OS_AUTH_URL
  #               user_domain_name: replace_me # $OS_USER_DOMAIN_NAME
  #               project_domain_name: replace_me # $OS_PROJECT_DOMAIN_NAME
  #               project_name: replace_me # $OS_PROJECT_DOMAIN_NAME
  #               username: replace_me # $OS_USERNAME
  #               password: replace_me # $OS_PASSWORD
  #             region_name: replace_me # $OS_REGION_NAME
  #             verify: false # boolean for server cert check
  #     network_id: 6bc87ec1-821b-4ee5-bd88-01151a45f3b3
  #     os_image_selector: # see TBC link to doc on OS image selectors
  #     image_key: replace_me # image used for CAPO CP & MD nodes (key of .Values.os_images)
  #     image_name: "" # image for this specific nodes
  #     server_group_id: 9f445ee4-1658-49d5-af99-a8b41906d252 # the OpenStack server group ID where these nodes belong to
  #     security_group_name: cef7168b-d62c-4ad6-a706-a477369155fc # the OpenStack security group name assigned to these nodes

images:
  kube_vip:
    repository: ghcr.io/kube-vip/kube-vip
    tag: v0.6.3
  kubectl:
    repository: registry.gitlab.com/sylva-projects/sylva-elements/container-images/kube-job
    tag: v1.0.5

ntp: {} # Let this parameter empty if you don't intend to configure NTP on machines
  # otherwise, provide following values
  #enabled: yes
  #servers: [] #list of NTP servers

proxies: # add your proxy settings if required
  https_proxy: ""
  http_proxy: ""
  no_proxy: ""

registry_mirrors: {} #add your local registry mirror to avoid rate limiting if required; provide the following values
  #default_settings:
    #capabilities: ["pull", "resolve"]
    #skip_verify: true
    #override_path: true
  #hosts_config:
    #docker.io:
    #  - mirror_url: https://<provide_your_url>
    #quay.io:
    #  - mirror_url: https://<provide_your_url>
    #registry.gitlab.com:
    #  - mirror_url: https://<provide_your_url>
    #k8s.gcr.io:
    #  - mirror_url: https://<provide_your_url>
    #ghcr.io:
    #  - mirror_url: https://<provide_your_url>
    #registry.k8s.io:
    #  - mirror_url: https://<provide_your_url>

kube_vip:
  bgp_lbs: {} # add your kube-vip config if required
    #bgp_routerid: # Typically the address of the local node
    #bgp_routerinterface: # Used to associate the routerID with the control plane's interface.
    #bgp_sourceif: # Determines which interface BGP should peer from
    #bgp_sourceip: # Determines which IP address BGP should peer from
    #bgp_as: # The AS we peer from
    #bgp_peers: # <address:AS:password:multihop> # Comma separated list of BGP peers
    #vip_cidr: # Defaults "32" # Used when advertising BGP addresses (typically as x.x.x.x/32)
metallb:
  l2_lbs: {} # add your metallb-l2 config if required
    #address_pools:
    #  - name: my-custom-pool
    #    addresses:
    #      - 10.122.22.151/32
    #      - 10.10.10.10-10.10.10.120
    #l2_options:
    #  advertisements:
    #    - node_selectors:
    #        - kubernetes.io/hostname: hostB #  to limit the set of nodes for a given advertisement, the node selector must be set
    #      interface: eth1  # interfaces selector can also be used together with node_selectors
    #      advertised_pools:
    #        - my-custom-pool # additional IP pools to be advertised to this peer

  bgp_lbs: {} # add your metallb-l3 config if required
    #l3_options:
    #  bgp_peers:
    #    - name: peerName
    #      local_asn: 64511  # example only, must be updated
    #      peer_asn: 64510  # example only, must be updated
    #      peer_address: 10.122.22.129
    #      advertised_pools:
    #        - lbpool  # default IP pool used for kube-api and ingress exposure
    #        - my-custom-pool  # additional IP pools to be advertised to this peer
    #address_pools:
    #  - name: my-custom-pool
    #    addresses:
    #      - 10.122.22.151/32
    #      - 10.10.10.10-10.10.10.120

default_node_class: generic
node_classes:
  generic:
    non_hugepages_minimum_memory_gb: 2
    kernel_cmdline:
      hugepages:
        enabled: false
        hugepagesz_2M: 0
        hugepagesz_1G: 0
        default_size: 1G
      extra_options: ""
    kubelet_extra_args: {}
    kubelet_config_file_options: {}
    nodeTaints: {}
    nodeLabels: {}
    nodeAnnotations: {}
    additional_commands:
      pre_bootstrap_commands: []
      post_bootstrap_commands: []

nodeDeletionTimeout: 0s

timeouts:
  cluster_delete_hook_job_timeout: 300
