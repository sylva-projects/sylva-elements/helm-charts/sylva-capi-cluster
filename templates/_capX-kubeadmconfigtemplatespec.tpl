{{- define "capd-KubeadmConfigTemplateSpec" }}
joinConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      # Source: https://github.com/kubernetes-sigs/cluster-api/blob/main/test/infrastructure/docker/examples/simple-cluster.yaml
      eviction-hard: nodefs.available<0%,nodefs.inodesFree<0%,imagefs.available<0%
    name: {{`'{{ ds.meta_data.name }}'`}}
ntp: {}
preKubeadmCommands: []
files: []
{{- end }}

{{- define "capo-KubeadmConfigTemplateSpec" }}
joinConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      provider-id: 'openstack:///{{`{{ ds.meta_data.uuid }}`}}'
    name: {{`'{{ ds.meta_data.name }}'`}}
ntp: {}
preKubeadmCommands: []
files: []
{{- end }}

{{- define "capv-KubeadmConfigTemplateSpec" }}
joinConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      cloud-provider: external
    name: {{`'{{ ds.meta_data.hostname }}'`}}
    criSocket: /var/run/containerd/containerd.sock
ntp: {}
preKubeadmCommands:
  - hostname {{`"{{ ds.meta_data.hostname }}"`}}
  - echo "::1         ipv6-localhost ipv6-loopback" >/etc/hosts
  - echo "127.0.0.1   localhost" >>/etc/hosts
  - echo "127.0.0.1   {{`{{ ds.meta_data.hostname }}`}}" >>/etc/hosts
  - echo {{`"{{ ds.meta_data.hostname }}"`}} >/etc/hostname
  - update-ca-certificates
files: []
{{- end }}

{{- define "capm3-KubeadmConfigTemplateSpec" }}
joinConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      provider-id: metal3://{{`{{ ds.meta_data.providerid }}`}}
    name: {{`'{{ ds.meta_data.name }}'`}}
ntp: {}
preKubeadmCommands:
  - hostname {{`"{{ ds.meta_data.local_hostname }}"`}}
  - | {{ tuple "false" .Values.capm3.dns_servers | include "shell-opensuse-dns" | nindent 4 }}
  {{- if .Values.enable_longhorn }}
  - | {{ include "shell-longhorn-mounts" . | nindent 4 }}
  {{- end }}
files: []
postKubeadmCommands:
  - | {{ include "clean-ironic-efi-boot-options" . | nindent 4 }}
{{- end }}
