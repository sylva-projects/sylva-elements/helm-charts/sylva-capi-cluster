{{- define "capd-kubeadmConfigSpec-CP" }}
initConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      eviction-hard: nodefs.available<0%,nodefs.inodesFree<0%,imagefs.available<0%
      register-with-taints: ""
joinConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      eviction-hard: nodefs.available<0%,nodefs.inodesFree<0%,imagefs.available<0%
      register-with-taints: ""
clusterConfiguration:
  apiServer:
    certSANs:
      - localhost
      - 127.0.0.1
  controllerManager:
    extraArgs:
      enable-hostpath-provisioner: "true"
ntp: {}
preKubeadmCommands: []
files: []
{{- end }}

{{- define "capo-kubeadmConfigSpec-CP" }}
initConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      provider-id: openstack:///{{`{{ ds.meta_data.uuid }}`}}
    name: {{`'{{ ds.meta_data.name }}'`}}
joinConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      provider-id: openstack:///{{`{{ ds.meta_data.uuid }}`}}
    name: {{`'{{ ds.meta_data.name }}'`}}
clusterConfiguration:
  apiServer:
    certSANs:
      {{- if and .Values.cluster_public_ip (not (eq .Values.cluster_public_ip .Values.cluster_virtual_ip)) }}
      - {{ .Values.cluster_virtual_ip }}
      {{- else }}
      []
      {{- end }}
ntp: {}
preKubeadmCommands: []
files: []
users: []
{{- end }}

{{- define "capv-kubeadmConfigSpec-CP" }}
initConfiguration:
  nodeRegistration:
    criSocket: /var/run/containerd/containerd.sock
    kubeletExtraArgs:
      cloud-provider: external
    name: {{`'{{ ds.meta_data.hostname }}'`}}
joinConfiguration:
  nodeRegistration:
    criSocket: /var/run/containerd/containerd.sock
    kubeletExtraArgs:
      cloud-provider: external
    name: {{`'{{ ds.meta_data.hostname }}'`}}
clusterConfiguration:
  apiServer:
    certSANs:
      - {{ .Values.cluster_virtual_ip }}
    extraArgs:
      cloud-provider: external
  controllerManager:
    extraArgs:
      cloud-provider: external
ntp: {}
preKubeadmCommands:
  - hostname {{`"{{ ds.meta_data.hostname }}"`}}
  - echo "::1         ipv6-localhost ipv6-loopback" >/etc/hosts
  - echo "127.0.0.1   localhost" >>/etc/hosts
  - echo "127.0.0.1  {{`{{ ds.meta_data.hostname }}`}}" >>/etc/hosts
  - echo {{`"{{ ds.meta_data.hostname }}"`}} >/etc/hostname
  - update-ca-certificates
files: []
users:
  - name: capv
    sshAuthorizedKeys:
      - {{ .Values.capv.ssh_key }}
    sudo: ALL=(ALL) NOPASSWD:ALL
{{- end }}

{{- define "capm3-kubeadmConfigSpec-CP" }}
initConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      provider-id: metal3://{{`{{ ds.meta_data.providerid }}`}}
    name: {{`'{{ ds.meta_data.local_hostname }}'`}}
joinConfiguration:
  nodeRegistration:
    kubeletExtraArgs:
      provider-id: metal3://{{`{{ ds.meta_data.providerid }}`}}
    name: {{`'{{ ds.meta_data.local_hostname }}'`}}
clusterConfiguration: {}
ntp: {}
preKubeadmCommands:
  - hostname {{`"{{ ds.meta_data.local_hostname }}"`}}
  - | {{ tuple "false" .Values.capm3.dns_servers | include "shell-opensuse-dns" | nindent 4 }}
  {{- if .Values.enable_longhorn }}
  - | {{ include "shell-longhorn-mounts" . | nindent 4 }}
  {{- end }}
files: []
postKubeadmCommands:
  - | {{ include "clean-ironic-efi-boot-options" . | nindent 4 }}
users: []
{{- end }}
