{{- define "OpenShiftAgentBootstrapConfigTemplateSpec" -}}
{{- $envAll := index . 0 -}}
{{- $machine_group_name := index . 2 -}}

nmStateConfigLabelSelector:
  matchLabels:
    sylva.org/nmstate: {{ printf "%s-%s" $envAll.Values.name $machine_group_name }}
{{- /* OpenshiftAssistedConfigTemplate has a additionalNTPSources, so set second arg to true */}}
{{ tuple $envAll true | include "openshiftCommonBootstrapSpec" }}
{{- end }}
