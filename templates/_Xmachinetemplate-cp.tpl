{{- define "XMachineTemplate-CP" }}
---
{{ if eq .Values.capi_providers.infra_provider "capd" }}
apiVersion: {{ include "getApiVersion" "DockerMachineTemplate" }}
kind: DockerMachineTemplate
metadata:
  name: {{ .Values.name }}-cp-{{ include "DockerMachineTemplateSpec" . | sha1sum | trunc 10 }}
  namespace: {{ .Release.Namespace }}
  annotations:
    "helm.sh/resource-policy": keep
spec:
  template:
    spec:
{{ include "DockerMachineTemplateSpec" . | indent 6 }}
{{ else if eq .Values.capi_providers.infra_provider "capo" }}
apiVersion: {{ include "getApiVersion" "OpenStackMachineTemplate" }}
kind: OpenStackMachineTemplate
metadata:
  name: {{ .Values.name }}-cp-{{ include "OpenStackMachineTemplateSpec-CP" . | sha1sum | trunc 10 }}
  namespace: {{ .Release.Namespace }}
  labels:
    role: control-plane
  annotations:
    "helm.sh/resource-policy": keep
spec:
  template:
    spec:
{{ include "OpenStackMachineTemplateSpec-CP" . | indent 6 }}
{{ else if eq .Values.capi_providers.infra_provider "capv" }}
apiVersion: {{ include "getApiVersion" "VSphereMachineTemplate" }}
kind: VSphereMachineTemplate
metadata:
  name: {{ .Values.name }}-cp-{{ include "VSphereMachineTemplateSpec-CP" . | sha1sum | trunc 10 }}
  namespace: {{ .Release.Namespace }}
  annotations:
    "helm.sh/resource-policy": keep
spec:
  template:
    spec:
{{ include "VSphereMachineTemplateSpec-CP" . | indent 6 }}
{{ else if eq .Values.capi_providers.infra_provider "capm3" }}
apiVersion: {{ include "getApiVersion" "Metal3MachineTemplate" }}
kind: Metal3MachineTemplate
metadata:
  name: {{ .Values.name }}-cp-{{ include "Metal3MachineTemplateSpec-CP" . | include "Metal3MachineTemplateSpec-remove-url-hostname" | sha1sum | trunc 10 }}
  namespace: {{ .Release.Namespace }}
  labels:
    role: control-plane
  annotations:
    "helm.sh/resource-policy": keep
spec:
{{ include "Metal3MachineTemplateSpec-CP" . | indent 2 }}
---
{{ include "Metal3DataTemplate-CP" . }}
{{- if ne .Values.agent_config_format "ignition" }}
{{/* The followings are for "cloud-config" systems, skip for "ignition" systems */}}
{{- if .Values.capm3.provisioning_pool_name }}
---
# NOTE: API explained at https://github.com/metal3-io/ip-address-manager/blob/main/docs/api.md#ippool
# TODO: review templating
apiVersion: {{ include "getApiVersion" "IPPool" }}
kind: IPPool
metadata:
  name: {{ printf "%s-%s" .Values.name .Values.capm3.provisioning_pool_name }}
  namespace: {{ .Release.Namespace }}
spec:
  clusterName: {{ .Values.name }}
  namePrefix: {{ .Values.name }}-prov
  gateway: {{ .Values.capm3.provisioning_pool_gateway }}
  pools:
  - start: {{ .Values.capm3.provisioning_pool_start }}
    end: {{ .Values.capm3.provisioning_pool_end }}
  prefix: {{ .Values.capm3.provisioning_pool_prefix }}
  {{- $envAll := . }}
  {{- $startIP := split "." .Values.capm3.provisioning_pool_start }}
  {{- $endIP := split "." .Values.capm3.provisioning_pool_end }}
  {{- $preAllocationsExist := false }}
  {{- range $bmh_name, $_ := $envAll.Values.baremetal_hosts }}
    {{- with index $envAll.Values.baremetal_hosts $bmh_name "ip_preallocations" }}
      {{- if .provisioning }}
        {{- $ip := .provisioning }}
        {{- $checkIP := split "." $ip }}
        {{- if and (eq $startIP._0 $checkIP._0) (eq $startIP._1 $checkIP._1) (eq $startIP._2 $checkIP._2) (and (ge (int $checkIP._3) (int $startIP._3)) (le (int $checkIP._3) (int $endIP._3))) }}
          {{- if not $preAllocationsExist }}
  preAllocations:
            {{- $preAllocationsExist = true }}
          {{- end }}
    {{ printf "%s-%s" (printf "%s-%s" $envAll.Values.name $bmh_name) (printf "%s-%s" $envAll.Values.name $envAll.Values.capm3.provisioning_pool_name) }}: {{ $ip }}
        {{ else }}
          {{ printf "[ERROR] Pre-allocated provisioning IP %s is not inside the provisioning network allocation range %s-%s " $ip $envAll.Values.capm3.provisioning_pool_start $envAll.Values.capm3.provisioning_pool_end | fail }}
        {{- end }}
      {{- end }}
    {{- end }}
  {{- end }}
{{- end }}
---
apiVersion: {{ include "getApiVersion" "IPPool" }}
kind: IPPool
metadata:
  name: {{ printf "%s-%s" .Values.name .Values.capm3.primary_pool_name }}
  namespace: {{ .Release.Namespace }}
spec:
  clusterName: {{ .Values.name }}
  namePrefix: {{ .Values.name }}-bmv4
  gateway: {{ .Values.capm3.primary_pool_gateway }}
  pools:
  - start: {{ .Values.capm3.primary_pool_start }}
    end: {{ .Values.capm3.primary_pool_end }}
  prefix: {{ .Values.capm3.primary_pool_prefix }}
  {{- $envAll := . }}
  {{- $startIP := split "." .Values.capm3.primary_pool_start }}
  {{- $endIP := split "." .Values.capm3.primary_pool_end }}
  {{- $preAllocationsExist := false }}
  {{- range $bmh_name, $_ := $envAll.Values.baremetal_hosts }}
    {{- with index $envAll.Values.baremetal_hosts $bmh_name "ip_preallocations" }}
      {{- if .primary }}
        {{- $ip := .primary }}
        {{- $checkIP := split "." $ip }}
        {{- if and (eq $startIP._0 $checkIP._0) (eq $startIP._1 $checkIP._1) (eq $startIP._2 $checkIP._2) (and (ge (int $checkIP._3) (int $startIP._3)) (le (int $checkIP._3) (int $endIP._3))) }}
          {{- if not $preAllocationsExist }}
  preAllocations:
            {{- $preAllocationsExist = true }}
          {{- end }}
    {{ printf "%s-%s" (printf "%s-%s" $envAll.Values.name $bmh_name) (printf "%s-%s" $envAll.Values.name $envAll.Values.capm3.primary_pool_name) }}: {{ $ip }}
        {{ else }}
          {{ printf "[ERROR] Pre-allocated primary IP %s is not inside the primary network allocation range %s-%s" $ip $envAll.Values.capm3.primary_pool_start $envAll.Values.capm3.primary_pool_end | fail }}
        {{- end }}
      {{- end }}
    {{- end }}
  {{- end }}
{{- end }} {{- /* if ne .Values.agent_config_format "ignition" */ -}}
{{ end }}
{{- end }}
