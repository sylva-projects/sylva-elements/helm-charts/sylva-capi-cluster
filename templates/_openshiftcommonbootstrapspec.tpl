{{- define "openshiftCommonBootstrapSpec" }}
  {{- $envAll := index . 0 -}}
  {{- $needNTP := index . 1 -}}
  {{- $useProxy := and $envAll.Values.proxies (not (empty $envAll.Values.proxies.http_proxy)) -}}
  {{- $addNTP := and $needNTP $envAll.Values.ntp $envAll.Values.ntp.enabled -}}
  {{- if and $addNTP (empty $envAll.Values.ntp.servers) }}
    {{- printf "[ERROR] ntp.servers.enabled is true but ntp.servers list is not provided" | fail }}
  {{- end }}
pullSecretRef:
  name: "{{ $envAll.Values.name }}-pull-secret"
sshAuthorizedKey: {{ $envAll.Values.openshift.sshAuthorizedKey }}
{{- if $useProxy }}
proxy:
  httpProxy: "{{ $envAll.Values.proxies.http_proxy }}"
  httpsProxy: "{{ $envAll.Values.proxies.http_proxy }}"
  noProxy: "{{ $envAll.Values.proxies.no_proxy }}"
{{- end }}
{{- if $addNTP }}
additionalNTPSources:
  {{- range  $envAll.Values.ntp.servers }}
  - {{ . | quote }}
  {{- end }}
{{- end }}
{{- end }}
