{{- /* This file contains named templates used to generate BMH networkdata in a DHCP-less context */}}


{{- /* This function retrieves the matching cp/md definition for a given bmh (according to labels)
It takes as parameter:
 - complete cluster definition (typically $envAll)
 - curent bmh name
 - current bmh definition
It will fail if:
 - no match is found
 - more than one cp/md matches
*/}}
{{- define "getMatchingMachineForBmh" }}
{{- $envAll := index . 0 -}}
{{- $bmh_name := index . 1 -}}
{{- $bmh_values := index . 2 -}}

{{- $matching_machine_group_values := dict }}
{{- $machine_groups := dict "control-plane" $envAll.Values.control_plane }}
{{- range $md_name, $md_values := $envAll.Values.machine_deployments }}
    {{- $md_merged_values := mergeOverwrite (deepCopy $envAll.Values.machine_deployment_default) $md_values }}
    {{- $machine_groups = merge $machine_groups (dict $md_name $md_merged_values) }}
{{- end }}

{{- range $machine_group_name, $machine_group_values := $machine_groups }}
  {{- $label_matches := 0 }}
  {{- range $label_key, $label_value := $machine_group_values.capm3.hostSelector.matchLabels }}
    {{- if (eq (dig "bmh_metadata" "labels" $label_key "" $bmh_values) $label_value) }}
      {{- $label_matches = add1 $label_matches }}
    {{- end }}
  {{- end }}
  {{- if eq $label_matches (len $machine_group_values.capm3.hostSelector.matchLabels) }}
    {{- if gt (len $matching_machine_group_values) 0 }}
      {{ printf "[ERROR] Multiple cp/md have hostSelector matching baremetalhost '%s'" $bmh_name | fail }}
    {{- else }}
      {{- $matching_machine_group_values = $machine_group_values }}
      {{- $_ := set $matching_machine_group_values "machineGroupName" $machine_group_name }}
    {{- end }}
  {{- end }}
{{- end }}
{{ if not $matching_machine_group_values }}
  {{ printf "[ERROR] Unable to find cp/md definition for baremetalhost %s" $bmh_name | fail }}
{{- end }}
{{- $matching_machine_group_values | toJson -}}
{{- end }}


{{- /* This function translates network configuration to nmstate format.
It takes as parameter:
 - a machine deployment definition  (typically $envAll.Values.machine_deployments.xxxx)
  / or control_plane definition ( typically $envAll.Values.control_plane)
 - the name of the baremetal host
 - a bmh definition (typically $envAll.Values.baremetal_hosts.xxx)
 - a global capm3 configuration (typically $envAll.Values.capm3)
*/}}
{{- define "translateNetworkConfigToNmstate" -}}
{{- $machine_group := index . 0 -}}
{{- $bmh_name := index . 1 -}}
{{- $bmh_values := index . 2 -}}
{{- $capm3_config := index . 3 -}}
{{- $capi_provider := index . 4 -}}
interfaces:
{{- range $machine_network_interface_name, $machine_network_interface_values := $machine_group.network_interfaces }}
  {{- /* If interface name was provided in interface_mappings, use it as identifier wherever we refer to physical interfaces */}}
  {{- $host_network_interface_name := dig "interface_mappings" $machine_network_interface_name "inspection_name" $machine_network_interface_name $bmh_values }}
  {{- if eq $machine_network_interface_values.type "phy" }}
    {{- $_ := set $machine_network_interface_values "type" "ethernet" }}
  {{- end }}
  - name: {{ $host_network_interface_name }}
    type: {{ $machine_network_interface_values.type }}
    state: up
    mtu: {{ $capm3_config.network_interfaces.mtu }}
  {{- $mac_address := dig "interface_mappings" $machine_network_interface_name "mac_address" false $bmh_values -}}
  {{- if and $mac_address (eq $machine_network_interface_values.type "ethernet") }}
    {{- /* If mac address was provided in interface_mappings, use it as identifier (see https://nmstate.io/features/mac_identifier.html) */}}
    {{- /* The following will cause issue for cabpoa */}}
    {{- if ne $capi_provider "cabpoa" }}
    identifier: mac-address
    mac-address: {{ $mac_address }}
    {{- end }}
  {{- end }}
  {{- if eq $machine_network_interface_values.type "bond" }}
    link-aggregation:
      mode: {{ $machine_network_interface_values.bond_mode | default $capm3_config.network_interfaces.bond_mode }}
      port:
      {{- range $bond_interface := $machine_network_interface_values.interfaces }}
      - {{ dig "interface_mappings" $bond_interface "name" $bond_interface $bmh_values }}
      {{- end }}
  {{- end }}
  {{- if and (eq $machine_network_interface_name $machine_group.capm3.provisioning_pool_interface)
             (dig "ip_preallocations" "provisioning" "" $bmh_values) }}
    ipv4:
      address:
        - ip: {{ $bmh_values.ip_preallocations.provisioning }}
          prefix-length: {{ $capm3_config.provisioning_pool_prefix | required "provisioning_pool_prefix not found" }}
      enabled: true
      dhcp: false
      auto-dns: false
  {{- else if and (eq $machine_network_interface_name $machine_group.capm3.primary_pool_interface)
             (dig "ip_preallocations" "primary" "" $bmh_values) }}
    ipv4:
      address:
        - ip: {{ $bmh_values.ip_preallocations.primary }}
          prefix-length: {{ $capm3_config.primary_pool_prefix }}
      enabled: true
      dhcp: false
      auto-dns: false
  {{- end }}
  {{- if hasKey $machine_network_interface_values "vlans" }}
    {{- range $vlan := $machine_network_interface_values.vlans }}
    {{- $machine_network_interface_vlan_name := printf "%s.%s" $host_network_interface_name ($vlan.id | toString) }}
  - name: {{ $machine_network_interface_vlan_name }}
    type: vlan
    state: up
    vlan:
      base-iface: {{ $host_network_interface_name }}
      id: {{ $vlan.id }}
      protocol: 802.1q
      {{- if or (eq $machine_network_interface_vlan_name $machine_group.capm3.provisioning_pool_interface)
          (eq $machine_network_interface_vlan_name $machine_group.capm3.primary_pool_interface) }}
    ipv4:
      address:
      {{- if eq $machine_network_interface_vlan_name $machine_group.capm3.provisioning_pool_interface }}
        - ip: {{ $bmh_values.ip_preallocations.provisioning }}
          prefix-length: {{ $capm3_config.provisioning_pool_prefix }}
      {{- else }}
        - ip: {{ $bmh_values.ip_preallocations.primary }}
          prefix-length: {{ $capm3_config.primary_pool_prefix }}
      enabled: true
      dhcp: false
      auto-dns: false
      {{- end }}
      {{- end }}
    {{- end }}
  {{- end }}
{{- end }}
dns-resolver:
  config:
    server: {{ $capm3_config.dns_servers | toYaml | nindent 6 }}
    {{- if or $capm3_config.networkData.primary_pool_interface $machine_group.capm3.networkData.primary_pool_interface $capm3_config.networkData.provisioning_pool_interface $machine_group.capm3.networkData.provisioning_pool_interface }}
routes:
  config:
      {{- $primary_interface_routes := deepCopy ($capm3_config.networkData.primary_pool_interface | default dict) -}}
      {{- tuple $primary_interface_routes $machine_group.capm3.networkData.primary_pool_interface | include "merge-append" }}
      {{- range $primary_route := $primary_interface_routes.routes }}
        {{- if hasKey $primary_route.gateway "fromIPPool" }}
          {{- fail (printf "'.gateway.fromIPPool' usage in \n%s\n\nis incompatible with 'baremetal_hosts.%s.ip_preallocations' usage. Please use '.gateway.string' to define route's next-hop" ($primary_interface_routes | toYaml) $bmh_name) -}}
        {{- else }}
    - destination: {{ printf "%s/%s" $primary_route.network ($primary_route.prefix | toString) }}
      next-hop-address: {{ $primary_route.gateway.string }}
      next-hop-interface: {{ $machine_group.capm3.primary_pool_interface }}
        {{- end }}
      {{- end }}
      {{- $provisioning_interface_routes := deepCopy ($capm3_config.networkData.provisioning_pool_interface | default dict) -}}
      {{- tuple $provisioning_interface_routes $machine_group.capm3.networkData.provisioning_pool_interface | include "merge-append" }}
      {{- range $provisioning_route := $provisioning_interface_routes.routes }}
        {{- if hasKey $provisioning_route.gateway "fromIPPool" }}
          {{- fail (printf "'.gateway.fromIPPool' usage in \n%s\n\nis incompatible with 'baremetal_hosts.%s.ip_preallocations' usage. Please use '.gateway.string' to define route's next-hop" ($provisioning_interface_routes | toYaml) $bmh_name) -}}
        {{- else }}
    - destination: {{ printf "%s/%s" $provisioning_route.network ($provisioning_route.prefix | toString) }}
      next-hop-address: {{ $provisioning_route.gateway.string }}
      next-hop-interface: {{ $machine_group.capm3.provisioning_pool_interface }}
        {{- end }}
      {{- end }}
    {{- else }}
routes:
  config:
    - destination: 0.0.0.0/0
      next-hop-address: '{{ $capm3_config.primary_pool_gateway }}'
      next-hop-interface: {{ $machine_group.capm3.primary_pool_interface }}
    {{- end }}
{{- end }}

{{- define "getNMstateConfigInterfaces" -}}
{{- $machine_group := index . 0 -}}
{{- $bmh_name := index . 1 -}}
{{- $bmh_values := index . 2 -}}
{{- $capm3_config := index . 3 -}}
{{- $num_of_interfaces := 0 -}}
{{- range $interface_name, $interface_values := $machine_group.network_interfaces }}
  {{- if eq $interface_values.type "ethernet" -}}
    {{- $mac_address := dig "interface_mappings" $interface_name "mac_address" "" $bmh_values }}
    {{- if $mac_address }}
- name: {{ $interface_name }}
  macAddress: {{ $mac_address }}
      {{- $num_of_interfaces = (add $num_of_interfaces 1) }}
    {{- end }}
  {{- end }}
{{- end }}
{{- if not $num_of_interfaces }}
  {{- fail (printf "[ERROR] interface_mappings.mac_address is missing under baremetal_host '%s'" $bmh_name) }}
{{- end }}
{{- end }}
