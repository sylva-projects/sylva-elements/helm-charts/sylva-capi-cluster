{{ define "OpenStackMachineTemplateSpec-CP" }}
flavor: {{ pluck "flavor_name" .Values.capo (.Values.control_plane.capo | default dict) | last }}
identityRef:
  cloudName: capo_cloud
  name: {{ .Values.name }}-capo-cloud-config
{{ include "capo-image" (tuple . ".capo/.control_plane.capo" (mergeOverwrite (deepCopy .Values.capo) (deepCopy .Values.control_plane.capo))) }}
sshKeyName: {{ .Values.capo.ssh_key_name }}
serverGroup:
  id: {{ .Values.control_plane.capo.server_group_id | required "you need to set a Nova server group UUID in control_plane.capo.server_group_id" }}
ports:
  - network:
      id: {{ .Values.capo.network_id }}
    securityGroups:
      - filter:
          name: k8s-cluster-{{ $.Release.Namespace }}-{{ .Values.name }}-secgroup-controlplane
    {{- range .Values.control_plane.capo.security_group_names }}
      - filter:
          name: {{ . }}
    {{ end }}
    {{- if .Values.control_plane.capo.additional_security_group_names }}
    {{- range .Values.control_plane.capo.additional_security_group_names }}
      - filter:
          name: {{ . }}
    {{ end }}
    {{- end }}
    allowedAddressPairs:
    {{- if .Values.capo.extra }}
      {{- if .Values.capo.extra.allowed_address_pairs }}
        {{- range .Values.capo.extra.allowed_address_pairs }}
      - ipAddress: {{ .ip | quote }}
        {{- if .mac }}
        macAddress: {{ .mac | lower | quote }}
        {{- end -}}
        {{- end -}}
      {{- end }}
    {{- end }}
      - ipAddress: {{ .Values.cluster_virtual_ip }}
    {{- if (or .Values.metallb.bgp_lbs .Values.metallb.l2_lbs) }}
      {{- range (concat (.Values.metallb.bgp_lbs.address_pools | default list) (.Values.metallb.l2_lbs.address_pools | default list) ) }}
        {{- range .addresses -}}
          {{- if regexMatch "^[0-9.]*-[0-9.]*$" . -}}
            {{- $range := split "-" . }}
            {{- $firstIPBits := split "." $range._0 -}}
            {{- $lastIPBits := split "." $range._1 }}
            {{- range untilStep ($firstIPBits._3 | int) ($lastIPBits._3 | add1 | int) 1 }}
      - ipAddress: {{ $firstIPBits._0 }}.{{ $firstIPBits._1 }}.{{ $firstIPBits._2 }}.{{ . }}
            {{- end -}}
          {{- else }}
            {{- if regexMatch "^[0-9.]*/[0-9.]*$" . }}
              {{- $range := split "/" . }}
              {{- $ip_add := $range._0 }}
              {{- if ne $ip_add $.Values.cluster_virtual_ip }}
      - ipAddress: {{ $ip_add }}
              {{- end }}
            {{- end }}
          {{- end }}
        {{- end }}
      {{- end }}
    {{- end }}
{{- range $cp_network_interface_name, $cp_network_interface_def := .Values.control_plane.network_interfaces }}
{{- $_ := set $cp_network_interface_def "description" $cp_network_interface_name }}
  - {{ $cp_network_interface_def | toYaml | nindent 4 }}
{{- end }}
{{ if (or .Values.capo.rootVolume .Values.control_plane.capo.rootVolume) }}
rootVolume:
  sizeGiB: {{ (mergeOverwrite (deepCopy .Values.capo.rootVolume) (.Values.control_plane.capo.rootVolume | default dict)).diskSize | int }}
  type: {{ (mergeOverwrite (deepCopy .Values.capo.rootVolume) (.Values.control_plane.capo.rootVolume | default dict)).volumeType }}
{{ end }}
{{ end }}
