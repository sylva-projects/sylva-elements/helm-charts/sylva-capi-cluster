{{/*
Create the registry hosts .toml file
*/}}
{{- define "registry_mirrors" -}}
{{- $registryMirrors := get .Values "registry_mirrors" | default dict -}}
{{- $defaultSettings := get $registryMirrors "default_settings" | default dict -}}
{{- range $registry, $mirrors := get $registryMirrors "hosts_config" | default dict }}
- path: /etc/containerd/registry.d/{{ $registry }}/hosts.toml
  owner: root
  permissions: "0644"
  content: |
    server = "https://{{ $registry }}"
    {{- range $_, $config := $mirrors }}
    [host."{{ $config.mirror_url }}"]
    {{- range $setting,$value := mergeOverwrite (deepCopy $defaultSettings) (get $config "registry_settings" | default dict) }}
      {{ $setting }} = {{ $value | toJson }}
{{- end }}
{{- end }}
{{- end }}
{{ end }}

{{/*
Create the RKE2 containerd config .toml file
*/}}
{{- define "rke2_config_toml" -}}
- path: /var/lib/rancher/rke2/agent/etc/containerd/config.toml.tmpl
  owner: "root:root"
  permissions: "0640"
  encoding: base64
  # Template copied from https://github.com/k3s-io/k3s/blob/master/pkg/agent/templates/templates_linux.go
  # adapted to use /etc/containerd/registry.d directory for registry configuration (to be consistent with kubeadm)
  content: |
{{ .Files.Get "files/rke2.config.toml" | b64enc | indent 4 }}
{{- end }}

{{/*
Create the Kubernetes manifest for kubeadm VIP .yaml file
*/}}
{{- define "kubernetes_kubeadm_vip" -}}
- path: /etc/kubernetes/manifests/kube-vip.yaml
  owner: root:root
  permissions: "0644"
  content: |
    apiVersion: v1
    kind: Pod
    metadata:
      creationTimestamp: null
      name: kube-vip
      namespace: kube-system
    spec:
      containers:
      - args:
        - manager
        env:
        - name: svc_enable
          value: "true"
        - name: svc_election
          value: "false"
        - name: svc_leasename
          value: "plndr-cp-lock"
        - name: vip_leasename
          value: "plndr-cp-lock"
        - name: vip_arp
          value: "true"
        - name: port
          value: "6443"
        - name: vip_cidr
          value: "32"
        - name: cp_enable
          value: "true"
        - name: cp_namespace
          value: kube-system
        - name: vip_ddns
          value: "false"
        - name: vip_leaderelection
          value: "true"
        - name: vip_leaseduration
          value: "5"
        - name: vip_renewdeadline
          value: "3"
        - name: vip_retryperiod
          value: "1"
        - name: address
          value: {{ .Values.cluster_virtual_ip }}
        {{- if .Values.kube_vip.bgp_lbs }}
        - name: bgp_enable
          value: "true"
        {{- range $key, $value := .Values.kube_vip.bgp_lbs }}
        - name: {{ $key }}
          value: {{ $value }}
        {{- end }}
        {{- end }}
        - name: prometheus_server
          value: :2112
        # Renovate Bot needs additional information to detect the kube-vip version:
        # renovate: registryUrl=https://ghcr.io image=kube-vip/kube-vip
        image: {{ .Values.images.kube_vip.repository }}:{{ .Values.images.kube_vip.tag }}
        imagePullPolicy: Always
        name: kube-vip
        resources: {}
        securityContext:
          capabilities:
            add:
            - NET_ADMIN
            - NET_RAW
        volumeMounts:
        - mountPath: /etc/kubernetes/admin.conf
          name: kubeconfig
        - mountPath: /etc/hosts
          name: etchosts
      hostNetwork: true
      volumes:
      - hostPath:
          path: {{ (include "k8s-version-match" (tuple ">=1.29.0" .Values.k8s_version) | eq "true") | ternary "/etc/kubernetes/kube-vip.admin.conf" "/etc/kubernetes/admin.conf" }}
        name: kubeconfig
      - hostPath:
          path: /etc/kube-vip.hosts
        name: etchosts
- path: /etc/kube-vip.hosts
  owner: "root:root"
  permissions: "0644"
  content: 127.0.0.1 localhost kubernetes
{{- end }}

{{/*
Create the Kubernetes manifest for RKE2 MetalLB .yaml file
*/}}
{{- define "kubernetes_rke2_metallb" -}}
- path: /opt/rke2/sylva-manifests/metallb.yaml
  owner: "root:root"
  permissions: "0644"
  content: |
      ---
      apiVersion: v1
      kind: Namespace
      metadata:
        name: metallb-system
        labels:
          pod-security.kubernetes.io/enforce: privileged
          pod-security.kubernetes.io/audit: privileged
          pod-security.kubernetes.io/warn: privileged
      ---
      apiVersion: helm.cattle.io/v1
      kind: HelmChart
      metadata:
        name: metallb
        namespace: metallb-system
      spec:
        bootstrap: true
        chart: {{ .Values.helm_oci_url.metallb | default "metallb" }}
        repo: {{ empty .Values.helm_oci_url.metallb | ternary "https://metallb.github.io/metallb" "" }}
        version: {{ .Values.helm_versions.metallb }}
        targetNamespace: metallb-system
        {{- if (or .Values.helm_extra_ca_certs.default .Values.helm_extra_ca_certs.metallb)  }}
        repoCA: |- {{ empty .Values.helm_extra_ca_certs.metallb | ternary .Values.helm_extra_ca_certs.default .Values.helm_extra_ca_certs.metallb | nindent 10 }}
        {{- end }}
        valuesContent: |-
          loadBalancerClass: "sylva.org/metallb-class"
          controller:
            nodeSelector:
              node-role.kubernetes.io/control-plane: "true"
            tolerations:
            - key: node.cloudprovider.kubernetes.io/uninitialized
              value: "true"
              effect: NoSchedule
            - effect: NoExecute
              key: node-role.kubernetes.io/etcd
            - effect: NoSchedule
              key: node-role.kubernetes.io/master
            - effect: NoSchedule
              key: node-role.kubernetes.io/control-plane
          speaker:
            frr:
              enabled: false
            tolerations:
            - key: node.cloudprovider.kubernetes.io/uninitialized
              value: "true"
              effect: NoSchedule
            - effect: NoExecute
              key: node-role.kubernetes.io/etcd
            - effect: NoSchedule
              key: node-role.kubernetes.io/master
            - effect: NoSchedule
              key: node-role.kubernetes.io/control-plane
- path: /opt/rke2/sylva-manifests/metallb-resources.yaml
  owner: "root:root"
  permissions: "0644"
  content: |
      ---
      apiVersion: metallb.io/v1beta1
      kind: IPAddressPool
      metadata:
        name: lbpool
        namespace: metallb-system
        labels:
          app.kubernetes.io/managed-by: Helm
        annotations:
          meta.helm.sh/release-name: metallb-resources
          meta.helm.sh/release-namespace: metallb-system
      spec:
        addresses:
          - {{ .Values.cluster_virtual_ip }}/32
      ---
      apiVersion: metallb.io/v1beta1
      kind: L2Advertisement
      metadata:
        name: l2advertisement
        namespace: metallb-system
        labels:
          app.kubernetes.io/managed-by: Helm
        annotations:
          meta.helm.sh/release-name: metallb-resources
          meta.helm.sh/release-namespace: metallb-system
      spec:
        ipAddressPools:
        - lbpool
{{- if eq .Values.capi_providers.infra_provider "capm3" }}
        interfaces: {{ .Values.cluster_primary_interfaces | default (list .Values.control_plane.capm3.primary_pool_interface) | required "we need one of: .cluster_primary_interfaces, control_plane.capm3.primary_pool_interface" | toYaml | nindent 10 }}
{{- end }}
        nodeSelectors:
          - matchLabels:
              node-role.kubernetes.io/control-plane: "true"
{{- if .Values.metallb.l2_lbs }}
  {{- range .Values.metallb.l2_lbs.address_pools }}
      ---
      apiVersion: metallb.io/v1beta1
      kind: IPAddressPool
      metadata:
        name: {{ .name }}
        namespace: metallb-system
        labels:
          app.kubernetes.io/managed-by: Helm
        annotations:
          meta.helm.sh/release-name: metallb-resources
          meta.helm.sh/release-namespace: metallb-system
      spec:
        addresses: {{ .addresses | toYaml | nindent 8 }}
  {{- end -}}
  {{- range .Values.metallb.l2_lbs.l2_options.advertisements }}
      ---
      apiVersion: metallb.io/v1beta1
      kind: L2Advertisement
      metadata:
        name: {{ .name }}
        namespace: metallb-system
        labels:
          app.kubernetes.io/managed-by: Helm
        annotations:
          meta.helm.sh/release-name: metallb-resources
          meta.helm.sh/release-namespace: metallb-system
      spec:
        ipAddressPools: {{ .advertised_pools | toYaml | nindent 8 }}
        {{- if .node_selectors }}
        nodeSelectors: {{ .node_selectors | toYaml | nindent 8 }}
        {{- end }}
        {{- if .interfaces }}
        interfaces:
          {{- range .interfaces }}
          - {{ . }}
          {{- end }}
        {{- end }}
  {{- end }}
{{- end }}
{{- end }}

{{/*
Create the Kubernetes manifest for RKE2 MetalLB-L3 .yaml file
*/}}
{{- define "kubernetes_rke2_metallb_l3" -}}
{{- if .Values.metallb.bgp_lbs }}
- path: /opt/rke2/sylva-manifests/metallb-l3.yaml
  owner: "root:root"
  permissions: "0644"
  content: |
    {{- range .Values.metallb.bgp_lbs.address_pools }}
      ---
      apiVersion: metallb.io/v1beta1
      kind: IPAddressPool
      metadata:
        name: {{ .name }}
        namespace: metallb-system
        labels:
          app.kubernetes.io/managed-by: Helm
        annotations:
          meta.helm.sh/release-name: metallb-resources
          meta.helm.sh/release-namespace: metallb-system
      spec:
        addresses: {{ .addresses | toYaml | nindent 8 }}
    {{- end -}}
    {{- range .Values.metallb.bgp_lbs.l3_options.bgp_peers }}
      {{- if hasKey . "password" }}
        {{- if hasKey . "passwordSecret" }}
          {{- printf "Specifying both 'password' and 'passwordSecret' for bgpPeer %s is forbidden. Please chose only one of them" .name | fail }}
        {{- else }}
      {{/* Create a secret containing the password to be used by bgppeer.spec.passwordSecret */}}
      ---
      apiVersion: v1
      kind: Secret
      metadata:
        name: bgppeer-{{ .name }}-pass
        namespace: metallb-system
        labels:
          app.kubernetes.io/managed-by: Helm
        annotations:
          meta.helm.sh/release-name: metallb-resources
          meta.helm.sh/release-namespace: metallb-system
      type: kubernetes.io/basic-auth
      data:
        password: {{ .password | b64enc }}
        {{- end }}
        {{- $_ := set . "passwordSecret" (printf "bgppeer-%s-pass" .name) }}
        {{- $_ := unset . "password" }}
      {{- end }}
      ---
      apiVersion: metallb.io/v1beta2
      kind: BGPPeer
      metadata:
        name: {{ .name }}
        namespace: metallb-system
        labels:
          app.kubernetes.io/managed-by: Helm
        annotations:
          meta.helm.sh/release-name: metallb-resources
          meta.helm.sh/release-namespace: metallb-system
      spec:
        myASN: {{ .local_asn }}
        peerASN: {{ .peer_asn }}
        peerAddress: {{ .peer_address }}
        {{- if hasKey . "passwordSecret" }}
        passwordSecret:
          name: {{ .passwordSecret }}
        {{- end }}
      ---
      apiVersion: metallb.io/v1beta1
      kind: BGPAdvertisement
      metadata:
        name: {{ .name }}
        namespace: metallb-system
        labels:
          app.kubernetes.io/managed-by: Helm
        annotations:
          meta.helm.sh/release-name: metallb-resources
          meta.helm.sh/release-namespace: metallb-system
      spec:
        ipAddressPools: {{ .advertised_pools | toYaml | nindent 8 }}
        peers:
        - {{ .name }}
    {{- end }}
{{- end }}
{{- end }}

{{/*
Create the Kubernetes manifest for RKE2 VIP .yaml file
*/}}
{{- define "kubernetes_rke2_vip" -}}
- path: /opt/rke2/sylva-manifests/cluster-vip.yaml
  owner: "root:root"
  permissions: "0644"
  content: |
      ---
      apiVersion: v1
      kind: Service
      metadata:
        name: cluster-vip
        namespace: kube-system
        annotations:
          metallb.universe.tf/allow-shared-ip: cluster-external-ip
      spec:
        # metallb will only start handling the VIP once it has a corresponding service with endpoints
        # but we don't want that the API access (6443) relies on kube-proxy, because on agent nodes,
        # kube-proxy uses rke2-proxy that may fall-back to the VIP to access the API,
        # which could create a dead lock if endpoints are not up-to-date
        type: LoadBalancer
        loadBalancerClass: "sylva.org/metallb-class"
        loadBalancerIP: {{ .Values.cluster_virtual_ip }}
        ports:
        - name: rke2-proxy
          port: 54321
          protocol: TCP
          targetPort: 54321
        selector:
          component: kube-apiserver
{{- end }}

{{/*
Create a service file to enable access to API service
*/}}
{{- define "kubernetes_rke2_api_vip_svc" -}}
- path: /etc/systemd/system/kube-api-vip-dnat.service
  owner: "root:root"
  permissions: "0644"
  content: |
    [Unit]
    Description=Enable access to k8S and rke2 APIs via cluster VIP
    After=network.target

    [Service]
    ExecStart=/usr/local/bin/kube-api-vip-dnat.sh

    [Install]
    WantedBy=multi-user.target
{{- end }}

{{/*
Create a service file to enable access to API service
*/}}
{{- define "kubernetes_rke2_api_vip_rule" -}}
- path: /usr/local/bin/kube-api-vip-dnat.sh
  owner: "root:root"
  permissions: "0755"
  content: |
    #!/bin/bash

    iptables -t nat -I PREROUTING 1 -d {{ .Values.cluster_virtual_ip }}/32 -p tcp --dport 6443 -m tcp -j DNAT --to-destination 127.0.0.1:6443
    iptables -t nat -I PREROUTING 1 -d {{ .Values.cluster_virtual_ip }}/32 -p tcp --dport 9345 -m tcp -j DNAT --to-destination 127.0.0.1:9345
{{- end }}

{{/*
Create the containerd proxy.conf file
*/}}
{{- define "containerd_proxy_conf" }}
- path: /etc/systemd/system/containerd.service.d/proxy.conf
  owner: root
  permissions: "0644"
  content: |
    [Service]
    Environment="HTTP_PROXY={{ .Values.proxies.http_proxy }}"
    Environment="HTTPS_PROXY={{ .Values.proxies.https_proxy }}"
    Environment="NO_PROXY={{ .Values.proxies.no_proxy }}"
{{- end }}

{{/*
Create the RKE2-server containerd proxy settings file
*/}}
{{- define "rke2_server_containerd_proxy" }}
- path: /etc/default/rke2-server
  owner: root:root
  permissions: "0644"
  content: |
    HTTP_PROXY={{ .Values.proxies.http_proxy }}
    HTTPS_PROXY={{ .Values.proxies.https_proxy }}
    NO_PROXY={{ .Values.proxies.no_proxy }}
{{- end }}

{{/*
Create the RKE2-agent containerd proxy settings file
*/}}
{{- define "rke2_agent_containerd_proxy" }}
- path: /etc/default/rke2-agent
  owner: root:root
  permissions: "0644"
  content: |
    HTTP_PROXY={{ .Values.proxies.http_proxy }}
    HTTPS_PROXY={{ .Values.proxies.https_proxy }}
    NO_PROXY={{ .Values.proxies.no_proxy }}
{{- end }}

{{/*
Create Calico helm charts
*/}}
{{- define "rke2_calico_helm_charts" }}
- path: /opt/rke2/sylva-manifests/rke2-calico-crd.yaml
  owner: root:root
  permissions: "0644"
  content: |
    apiVersion: helm.cattle.io/v1
    kind: HelmChart
    metadata:
      name: rke2-calico-crd
      namespace: kube-system
    spec:
      bootstrap: true
      chart: {{ index .Values.helm_oci_url "calico-crd" | default "rke2-calico-crd" }}
      repo: {{ empty (index .Values.helm_oci_url "calico-crd") | ternary "https://rke2-charts.rancher.io" "" }}
      version: {{ index .Values.helm_versions "calico-crd" }}
      targetNamespace: kube-system
      {{- if (or .Values.helm_extra_ca_certs.default .Values.helm_extra_ca_certs.calico) }}
      repoCA: |- {{ empty .Values.helm_extra_ca_certs.calico | ternary .Values.helm_extra_ca_certs.default .Values.helm_extra_ca_certs.calico | nindent 10 }}
      {{- end }}
- path: /opt/rke2/sylva-manifests/rke2-calico.yaml
  owner: root:root
  permissions: "0644"
  content: |
    apiVersion: helm.cattle.io/v1
    kind: HelmChart
    metadata:
      name: rke2-calico
      namespace: kube-system
    spec:
      bootstrap: true
      chart: {{ .Values.helm_oci_url.calico | default "rke2-calico" }}
      repo: {{ empty .Values.helm_oci_url.calico | ternary "https://rke2-charts.rancher.io" "" }}
      version: {{ .Values.helm_versions.calico }}
      targetNamespace: kube-system
      {{- if (or .Values.helm_extra_ca_certs.default .Values.helm_extra_ca_certs.calico) }}
      repoCA: |- {{ empty .Values.helm_extra_ca_certs.calico | ternary .Values.helm_extra_ca_certs.default .Values.helm_extra_ca_certs.calico | nindent 10 }}
      {{- end }}
{{- end }}

{{/*
Configure rke2-calico HelmChart to tolerate node.cloudprovider.kubernetes.io/uninitialized and node.kubernetes.io/not-ready,
enable wireguard, set mtu for capm3 clusters and configure the nodeAddressAutodetection
*/}}
{{- define "rke2_calico_helm_chart_config" }}
- path: /opt/rke2/sylva-manifests/rke2-calico-config.yaml
  owner: root:root
  permissions: "0644"
  content: |
    apiVersion: helm.cattle.io/v1
    kind: HelmChartConfig
    metadata:
      name: rke2-calico
      namespace: kube-system
    spec:
      valuesContent: |-
        {{ .Values.cni.calico.helm_values | toYaml | nindent 8 }}
{{- end }}

{{/*
Configure rke2-coredns HelmChart to tolerate node.cloudprovider.kubernetes.io/uninitialized
*/}}
{{- define "rke2_coredns_helm_chart_config" }}
- path: /var/lib/rancher/rke2/server/manifests/rke2-coredns-toleration.yaml
  owner: root:root
  permissions: "0644"
  content: |
    apiVersion: helm.cattle.io/v1
    kind: HelmChartConfig
    metadata:
      name: rke2-coredns
      namespace: kube-system
    spec:
      valuesContent: |-
        tolerations:
          - key: "node.cloudprovider.kubernetes.io/uninitialized"
            effect: "NoSchedule"
            value: "true"
{{- end }}

{{/*
Configure rke2-kubelet-config-file
*/}}
{{- define "rke2-kubelet-config-file" }}
- path: /var/lib/rancher/rke2/server/kubelet-configuration-file.yaml
  owner: root:root
  permissions: "0644"
  content: |
    ---
    apiVersion: kubelet.config.k8s.io/v1beta1
    kind: KubeletConfiguration
    {{ if . }}
    {{ . | toYaml | nindent 4 }}
    {{ end }}
{{- end }}


{{/*
Create the machine files defined from external files through chart values .additional_files
*/}}
{{- define "additional_files" -}}
{{- $envAll := index . 0 -}}
{{- $additional_files := index . 1 -}}
{{- range $file, $file_specs := $additional_files }}
- owner: {{ $file_specs.owner }}
  permissions: {{ $file_specs.permissions | quote }}
  {{- if $file_specs.path }}
  path: {{ $file_specs.path }}
  {{- else }}
  path: {{ $file }}
  {{- end }}
  {{- if $file_specs.content }}
    {{- if $file_specs.encoding }}
  encoding: {{ $file_specs.encoding }}
  content: |
{{ $file_specs.content | indent 4 }}
    {{- else }}
      {{/*********** protect $file_specs.content from unwanted evaluation
      Helm supports only Base64 & Base32 encoding, https://helm.sh/docs/chart_template_guide/function_list/#encoding-functions
      out of which CABPK & CABPR only support base64 */}}
  encoding: base64
  content: |
{{ $file_specs.content | b64enc | indent 4 }}
    {{- end }}
  {{- else if $file_specs.content_file }}
    {{- if not ($envAll.Files.Glob ($file_specs.content_file | clean)) }}
      {{- fail (printf "no such file exists: %s" $file_specs.content_file) -}}
    {{- end }}
    {{- if $file_specs.encoding }}
  encoding: {{ $file_specs.encoding }}
  content: |
{{ $envAll.Files.Get $file_specs.content_file | indent 4 }}
    {{- else }}
      {{/*********** protect $file_specs.content_file contents from unwanted evaluation
      Helm supports only Base64 & Base32 encoding, https://helm.sh/docs/chart_template_guide/function_list/#encoding-functions
      out of which CABPK & CABPR only support base64 */}}
  encoding: base64
  content: |
{{ $envAll.Files.Get $file_specs.content_file | b64enc | indent 4 }}
    {{- end }}
  {{- else if $file_specs.content_k8s_secret }}
  contentFrom:
    secret: {{ $file_specs.content_k8s_secret | toJson }}
  {{- end }}
{{- end }}
{{ end }}


{{/*
Create the kubernetes audit policy file defined from  policy rules defines in user values
*/}}
{{- define "audit_policy_config_file" -}}
{{ if .Values.audit_policies }}
- path: /etc/rancher/rke2/audit-policy.yaml
  owner: root:root
  permissions: "0400"
  content: |
    apiVersion: audit.k8s.io/v1
    kind: Policy
    {{ if .Values.audit_policies.omitStages }}
    omitStages: {{ .Values.audit_policies.omitStages | toYaml | nindent 4 }}
    {{- end }}
    rules:
    {{ .Values.audit_policies.rules | toYaml | trim | nindent 4 }}
{{- end }}
{{- end }}

{{/*
Create a script to install the CNI manifests and Metallb
*/}}
{{- define "sylva-manifests-install" -}}
- path: /opt/rke2/sylva-manifests/manifests-install.sh
  owner: root:root
  permissions: "0755"
  content: |
    #!/bin/bash

    # on installation of the _first_ node, we install MetalLB
    # (nodes that come up after the first one will have "server: https://<VIP>:9345" in /etc/rancher/rke2/config.yaml)
    kubectl="/var/lib/rancher/rke2/bin/kubectl --kubeconfig /etc/rancher/rke2/rke2.yaml"
    if ! grep -q "^server: " /etc/rancher/rke2/config.yaml; then
        echo "Installing CNI"
        {{- if (eq .Values.cni.default_provider "calico") }}
        $kubectl apply -f /opt/rke2/sylva-manifests/rke2-calico-crd.yaml
        while ! $kubectl get -n kube-system job helm-install-rke2-calico-crd ; do sleep 5; done
        $kubectl -n kube-system wait job helm-install-rke2-calico-crd --for=condition=Complete --timeout=300s
        {{- end }}
        $kubectl apply -f /opt/rke2/sylva-manifests/rke2-{{ .Values.cni.default_provider}}-config.yaml
        $kubectl apply -f /opt/rke2/sylva-manifests/rke2-{{ .Values.cni.default_provider }}.yaml
        echo "Waiting for Calico to be ready..."
        while ! $kubectl get -n kube-system job helm-install-rke2-{{ .Values.cni.default_provider }} ; do sleep 5; done
        $kubectl -n kube-system wait job helm-install-rke2-{{ .Values.cni.default_provider }} --for=condition=Complete --timeout=600s
        echo "Installing Metallb"
        $kubectl apply -f /opt/rke2/sylva-manifests/metallb.yaml
        echo "Waiting for Metallb to be ready..."
        while ! $kubectl get -n metallb-system service metallb-webhook-service; do sleep 5; done
        $kubectl -n metallb-system wait deployment metallb-controller --for=jsonpath='{.status.readyReplicas}'=1 --timeout=300s
        echo "Create/update metallb resources to be controlled by Helm"
        $kubectl apply -f /opt/rke2/sylva-manifests/metallb-resources.yaml
        {{- if .Values.metallb.bgp_lbs }}
        echo "Setting up MetalLB BGP sessions"
        $kubectl apply -f /opt/rke2/sylva-manifests/metallb-l3.yaml
        {{- end }}
        echo "Creating kubernetes-vip service"
        $kubectl apply -f /opt/rke2/sylva-manifests/cluster-vip.yaml
        echo "Cleanup helm.cattle.io resources for metallb and calico"
        $kubectl -n metallb-system delete cm chart-content-metallb
        $kubectl -n metallb-system delete secret chart-values-metallb
        $kubectl -n metallb-system delete secret -l name=metallb,owner=helm
        $kubectl -n metallb-system patch helmchart --type merge -p '{"metadata":{"finalizers": [], "annotations":{"helmcharts.helm.cattle.io/unmanaged":""}}}' metallb
        $kubectl -n metallb-system delete helmchart metallb
        $kubectl -n kube-system delete cm chart-content-rke2-calico chart-content-rke2-calico-crd
        $kubectl -n kube-system delete secret chart-values-rke2-calico chart-values-rke2-calico-crd
        $kubectl -n kube-system delete secret -l 'name in (rke2-calico, rke2-calico-crd)',owner=helm
        $kubectl -n kube-system patch helmchart --type merge -p '{"metadata":{"finalizers": [], "annotations":{"helmcharts.helm.cattle.io/unmanaged":""}}}' rke2-calico rke2-calico-crd
        $kubectl -n kube-system delete helmchart.helm.cattle.io rke2-calico
        $kubectl annotate felixconfiguration default meta.helm.sh/release-name=rke2-calico
        $kubectl annotate felixconfiguration default  meta.helm.sh/release-namespace=kube-system
        $kubectl label  felixconfiguration default  app.kubernetes.io/managed-by=Helm
    fi
{{- end }}
